import React from 'react';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import PropTypes from 'prop-types';
import RegisterModal from '../../containers/Register/RegisterModal/RegisterModal';

const Layout = (props) => {
  return (
    <React.Fragment>
      <RegisterModal />
      <Header/>
      <main style={{paddingTop: '90px'}}>{props.children}</main>
      <Footer />
    </React.Fragment>
  );
}

Layout.propTypes = {
  children: PropTypes.node.isRequired
}


 
export default Layout;