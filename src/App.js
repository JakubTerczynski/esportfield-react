import React, {Suspense, useEffect} from 'react';
import {Route, Switch, withRouter} from 'react-router-dom';
import Layout from './hoc/Layout/Layout';
import Dashboard from './components/Dashboard/Dashboard';
import Spinner from './components/UI/Spinner/Spinner';
import {connect} from 'react-redux';
import * as actions from './store/actions/index';
import Logout from './containers/Auth/Logout/Logout';
import PropTypes from 'prop-types';

const Login = React.lazy(() => {
  return import('./containers/Auth/Login/Login');
});

const Register = React.lazy(() => {
  return import('./containers/Register/RegisterForm/RegisterForm');
});

const VideoAnalysis = React.lazy(() => {
  return import('./containers/VideoAnalysis/VideoAnalysis');
});

const VideoAnalysisDisabled = React.lazy(() => {
  return import('./containers/VideoAnalysis/VideoAnalysisDisabled/VideoAnalysisDisabled');
});

const App = props => {
  const {isAuth, onTryAutoLogin, onTryAutoSetLanguage,  onGetGames} = props;

  useEffect(() => {
    onTryAutoLogin();
  }, [onTryAutoLogin]);

  useEffect(() => {
    onTryAutoSetLanguage();
  }, [onTryAutoSetLanguage]);

  useEffect(() => {
    onGetGames();
  }, [onGetGames]);

  let routes = (
    <Switch>
      <Route path="/register" render={(props) => <Register {...props} />} />
      <Route path="/login" render={(props) => <Login {...props} />} />
      <Route path="/" exact component={Dashboard} />
    </Switch>
  );

  if (isAuth) {
    routes = (
      <Switch>
        <Route path="/video-analysis/:id" render={(props) => <VideoAnalysis {...props} />} />
        <Route path="/video-analysis-view/:id" render={(props) => <VideoAnalysisDisabled {...props} />} />
        <Route path="/register" render={(props) => <Register {...props} />} />
        <Route path="/logout" component={Logout} />
        <Route path="/login" render={(props) => <Login {...props} />} />
        <Route path="/" exact component={Dashboard} />
      </Switch>
    );
  }

  return (
    <div>
      <Layout>
        <Suspense fallback={<Spinner />}>{routes}</Suspense>
      </Layout>
    </div>
  );
}

App.propTypes = {
  isAuth: PropTypes.bool,
  onTryAutoLogin: PropTypes.func,
  onTryAutoSetLanguage: PropTypes.func,
  onGetGames: PropTypes.func
}

const mapStateToProps = state => {
  return {
    isAuth: state.auth.token !== null
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoLogin: () => dispatch(actions.autoLogin()),
    onTryAutoSetLanguage: () => dispatch(actions.autoSetLanguageInit()),
    onGetGames: () => dispatch(actions.getGames())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));