import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import authReducer from './store/reducers/auth';
import createSagaMiddleware from 'redux-saga';
import { watchAuth, watchLanguage, watchRegister, watchGames, watchUserRoles, watchVideoAnalysis } from './store/sagas/index';
import languageReducer from './store/reducers/language';
import registerReducer from './store/reducers/register';
import gamesReducer from './store/reducers/games';
import userRolesReducer from './store/reducers/userRoles';
import videoAnalysisReducer from './store/reducers/videoAnalysis';
import axios from './axios-instance';

const composeEnhancers = 
  process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || 
  compose;

const rootReducer = combineReducers({
  auth: authReducer,
  lang: languageReducer,
  register: registerReducer,
  games: gamesReducer,
  userRoles: userRolesReducer,
  videoAnalysis: videoAnalysisReducer
});

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(sagaMiddleware)));

sagaMiddleware.run(watchAuth);
sagaMiddleware.run(watchLanguage);
sagaMiddleware.run(watchRegister);
sagaMiddleware.run(watchGames);
sagaMiddleware.run(watchUserRoles);
sagaMiddleware.run(watchVideoAnalysis);

axios.interceptors.request.use(config => {
  const token = store.getState().auth.token;
  const email = store.getState().auth.email;
  
  config.headers.common['X-User-Email'] = email;
  config.headers.common['X-User-Token'] = token;

  return config;
});

export default store;