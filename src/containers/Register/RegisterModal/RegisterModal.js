import React, { useState } from 'react';
import { Dialog, Grid, Card, Link } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { UserRoles } from '../../../shared/dicts/userRoles';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import { NavLink, Redirect } from 'react-router-dom';
import ButtonSecondary from '../../../components/UI/Buttons/ButtonSecondary/ButtonSecondary';
import playerImg from '../../../assets/images/player.svg';
import trainImg from '../../../assets/images/train.svg';
import organizationImg from '../../../assets/images/organization.svg';

const RegisterModal = ({ intl, ...props }) => {
  const [redirect, setRedirect] = useState(null);

  const texts = {
    registerAs: intl.formatMessage({
      id: 'register.as'
    }),
    register: intl.formatMessage({
      id: 'header.register'
    }),
    player: intl.formatMessage({
      id: 'player'
    }),
    coach: intl.formatMessage({
      id: 'coach'
    }),
    organizationAgent: intl.formatMessage({
      id: 'organization.agent'
    }),
    haveAccount: intl.formatMessage({
      id: 'account.doYouHave'
    }),
    login: intl.formatMessage({
      id: 'common.login'
    })
  };

  const redirectToRegisterHandler = (userRole) => {
    setRedirect(<Redirect to={{
      pathname: '/register',
      state: { userRole: userRole }
    }} />);
    props.onCloseRegisterModal();
  }

  return (
    <React.Fragment>
      {redirect}
      <Dialog
        open={props.isOpen}
        onClose={props.onCloseRegisterModal}
        maxWidth={'md'}
        fullWidth={true}
        className="Modal">
        <Card style={{ textAlign: 'center' }}>
          <div style={{ padding: '30px' }}>
            <h1 className="header-text-bold font-24px">{texts.registerAs}</h1>
            <br /><br />
            <Grid container>
              <Grid item xs={4}>
                <img src={playerImg} alt="Player" />
                <p className="header-text-bold font-17px"
                  style={{ padding: '10px 0px' }}>{texts.player}</p>
                <ButtonSecondary
                  onClick={() => redirectToRegisterHandler(UserRoles.player)}>{texts.register}</ButtonSecondary>
              </Grid>
              <Grid item xs={4}>
                <img src={trainImg} alt="Coach" />
                <p className="header-text-bold font-17px"
                  style={{ padding: '10px 0px' }}>{texts.coach}</p>
                <ButtonSecondary
                  onClick={() => redirectToRegisterHandler(UserRoles.coach)}>{texts.register}</ButtonSecondary>
              </Grid>
              <Grid item xs={4}>
                <img src={organizationImg} alt="Organization" />
                <p className="header-text-bold font-17px"
                  style={{ padding: '10px 0px' }}>{texts.organizationAgent}</p>
                <ButtonSecondary>{texts.register}</ButtonSecondary>
              </Grid>
            </Grid>
          </div>
          <hr style={{ border: '0.5px solid #5F5F5F', margin: '10px 0px' }} />
          <p className="header-text-regular font-14px"
            style={{ padding: '30px' }}>
            <span style={{ opacity: 0.5 }}>{texts.haveAccount}?</span>
            &nbsp;
            <NavLink to="/login">
              <Link
                component="button"
                variant="body2"
                onClick={props.onCloseRegisterModal}
                style={{ color: '#0E76BC' }}>
                {texts.login}</Link>
            </NavLink>
          </p>
        </Card>
      </Dialog>
    </React.Fragment>
  );
}

RegisterModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onCloseRegisterModal: PropTypes.func
};

const mapStateToProps = state => {
  return {
    isOpen: state.register.isOpen
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onCloseRegisterModal: () => dispatch(actions.closeRegisterModal()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(RegisterModal));