import React, { useState, useEffect } from 'react';
import { Grid, Card, Link, makeStyles, Checkbox, FormControlLabel } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import { Redirect, NavLink } from 'react-router-dom';
import Spinner from '../../../components/UI/Spinner/Spinner';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import { UserRoles } from '../../../shared/dicts/userRoles';
import GameSelect from '../../../components/GameSelect/GameSelect';
import registerClasses from '../Register.module.scss';
import ButtonPrimary from '../../../components/UI/Buttons/ButtonPrimary/ButtonPrimary';
import StyledInput from '../../../components/UI/StyledInput/StyledInput';

const useStyles = makeStyles(theme => ({
  label: {
    fontFamily: 'Poppins-Regular',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '11px',
    lineHeight: '13px',
    color: 'grey',
    textAlign: 'left'
  }
}));

const RegisterForm = ({ intl, ...props }) => {
  const [gameId, setGameId] = useState(0);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');
  const [nickname, setNickname] = useState('');
  const uiClasses = useStyles();
  const {isAuthenticated, error, loading, onRegister, onGetUserRoles, location, userRoles} = props;

  useEffect(() => {
    onGetUserRoles();
  }, [onGetUserRoles]);

  const userRole = location.state.userRole;
  const texts = {
    createPlayerAccount: intl.formatMessage({
      id: 'account.createPlayerAccount'
    }),
    createCoachAccount: intl.formatMessage({
      id: 'account.createCoachAccount'
    }),
    createAccount: intl.formatMessage({
      id: 'account.createAccount'
    }),
    register: intl.formatMessage({
      id: 'header.register'
    }),
    email: intl.formatMessage({
      id: 'common.email'
    }),
    nickname: intl.formatMessage({
      id: 'common.nickname'
    }),
    password: intl.formatMessage({
      id: 'common.password'
    }),
    passwordConfirmation: intl.formatMessage({
      id: 'common.passwordConfirmation'
    }),
    haveAccount: intl.formatMessage({
      id: 'account.doYouHave'
    }),
    login: intl.formatMessage({
      id: 'common.login'
    }),
    agreement: intl.formatMessage({
      id: 'agreement'
    })
  };

  const gameIdChangeHandler = (event) => {
    setGameId(event.target.value);
  }

  const emailChangeHandler = (event) => {
    setEmail(event.target.value);
  }

  const nicknameChangeHandler = (event) => {
    setNickname(event.target.value);
  }

  const passwordChangeHandler = (event) => {
    setPassword(event.target.value);
  }

  const passwordConfirmationChangeHandler = (event) => {
    setPasswordConfirmation(event.target.value);
  }

  const registerHandler = (event) => {
    event.preventDefault();
    const userRoleId = userRoles.find(role => role.name === userRole).id;
    const userData = {
      email: email,
      nickname: nickname,
      password: password,
      password_confirmation: passwordConfirmation,
      user_role_id: userRoleId,
      game_id: gameId
    };
    onRegister(userData);
  }

  let errorMessage = null;
  if (error) {
    errorMessage = (
      <p style={{ color: 'red', fontWeight: 'bold' }}>{error}</p>
    );
  }

  let redirect = null;
  if (isAuthenticated) {
    redirect = <Redirect to="/" />;
  }

  let form = (
    <form onSubmit={registerHandler}>
      <GameSelect
        value={gameId}
        onChange={gameIdChangeHandler}
        variant="outlined" />
      <br />
      <StyledInput
        label={texts.email}
        type="email"
        name="email"
        value={email}
        onChange={emailChangeHandler}/>
      <br />
      <StyledInput
        label={texts.nickname}
        type="text"
        value={nickname}
        onChange={nicknameChangeHandler}/>
      <br />
      <StyledInput
        label={texts.password}
        type="password"
        value={password}
        onChange={passwordChangeHandler} />
      <br />
      <StyledInput
        label={texts.passwordConfirmation}
        type="password"
       value={passwordConfirmation}
        onChange={passwordConfirmationChangeHandler} />
      <br /><br />
      <FormControlLabel
        label={<p className={uiClasses.label}>{texts.agreement}</p>}
        control={
          <Checkbox onChange={() => {}}
          value={false}
          style={{color: '#0E76BC'}} />}/>
      <br /><br />
      <ButtonPrimary type="submit" isfullwidth="true"
        style={{margin: '10px 0px'}}>{texts.register}</ButtonPrimary>
    </form>
  );
  if (loading) {
    form = <Spinner />;
  }
  let headerText = '';
  if (userRole === UserRoles.player) {
    headerText = texts.createPlayerAccount;
  } else if (userRole === UserRoles.coach) {
    headerText = texts.createCoachAccount;
  }

  return (
    <Grid container justify="center" style={{ margin: '60px 0px' }}>
      <Grid item xs={12} sm={4}>
        <Card className={registerClasses.RegisterCard}>
          <br />
          <h1 className="header-text-bold font-24px">{headerText}</h1>
          <p className="header-text-regular font-14px" style={{opacity: 0.5}}>
            {texts.createAccount}
          </p>
          <div className={registerClasses.Padding40px}>
            {redirect}
            {errorMessage}
            {form}
          </div>
          <hr style={{ border: '0.5px solid #5F5F5F' }} />
          <p className="header-text-regular font-14px"
            style={{ padding: '30px' }}>
            <span style={{ opacity: 0.5 }}>{texts.haveAccount}?</span>
            &nbsp;
            <NavLink to="/login">
              <Link
                component="button"
                variant="body2"
                style={{ color: '#0E76BC' }}>
                {texts.login}</Link>
            </NavLink>
          </p>
        </Card>
      </Grid>
    </Grid>
  );
}

RegisterForm.propTypes = {
  isAuthenticated: PropTypes.bool,
  error: PropTypes.string,
  loading: PropTypes.bool,
  onRegister: PropTypes.func,
  onGetUserRoles: PropTypes.func,
  location: PropTypes.shape({
    state: PropTypes.shape({
      userRole: PropTypes.oneOf([UserRoles.player, UserRoles.coach])
    })
  }).isRequired,
  userRoles: PropTypes.array
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null,
    error: state.register.error,
    loading: state.register.loading,
    userRoles: state.userRoles.userRoles
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onRegister: (userData) => dispatch(actions.register(userData)),
    onGetUserRoles: () => dispatch(actions.getUserRoles())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(RegisterForm));