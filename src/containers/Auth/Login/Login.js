import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import Spinner from '../../../components/UI/Spinner/Spinner';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import { Card, Grid, Link } from '@material-ui/core';
import classes from './Login.module.scss';
import ButtonPrimary from '../../../components/UI/Buttons/ButtonPrimary/ButtonPrimary';
import clsx from 'clsx';
import StyledInput from '../../../components/UI/StyledInput/StyledInput';

const Login = ({ intl, ...props }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const texts = {
    loginButton: intl.formatMessage({
      id: 'header.login'
    }),
    email: intl.formatMessage({
      id: 'common.email'
    }),
    password: intl.formatMessage({
      id: 'common.password'
    }),
    login: intl.formatMessage({
      id: 'login'
    }),
    dontYouHaveAccount: intl.formatMessage({
      id: 'account.dontYouHave'
    }),
    register: intl.formatMessage({
      id: 'header.register'
    })
  };

  const emailChangeHandler = (event) => {
    setEmail(event.target.value);
  }

  const passwordChangeHandler = (event) => {
    setPassword(event.target.value);
  }

  const loginHandler = (event) => {
    event.preventDefault()
    props.onLogin(email, password);
  }

  let errorMessage = null;
  if (props.error) {
    errorMessage = (
      <p style={{ color: 'red', fontWeight: 'bold' }}>{props.error}</p>
    );
  }

  let authRedirect = null;
  if (props.isAuthenticated) {
    authRedirect = <Redirect to="/" />;
  }

  let form = (
    <form onSubmit={loginHandler}>
      <StyledInput
        label={texts.email}
        type="email"
        name="email"
        value={email}
        onChange={emailChangeHandler}/>
      <br />
      <StyledInput
        label={texts.password}
        type="password"
        value={password}
        onChange={passwordChangeHandler}/>
      <br /><br /><br />
      <ButtonPrimary type="submit" isfullwidth="true">{texts.loginButton}</ButtonPrimary>
    </form>
  );
  if (props.loading) {
    form = <Spinner />;
  }

  return (
    <Grid container justify="center" style={{margin: '60px 0px'}}>
      <Grid item xs={12} sm={4}>
        <Card className={classes.Card} >
          <br />
          <h1 className="header-text-bold font-24px">{texts.login}</h1>
          <div className={classes.Padding40px}>
            {authRedirect}
            {errorMessage}
            {form}
          </div>
          <hr style={{ border: '0.5px solid #5F5F5F' }} />
          <p className={clsx("header-text-regular font-14px", classes.Padding20px)}>
            <span style={{opacity: 0.5}}>{texts.dontYouHaveAccount}?</span>
            &nbsp;
            <Link
              component="button"
              variant="body2"
              onClick={props.onOpenRegisterModal}
              style={{color: '#0E76BC'}}>
              {texts.register}</Link>
          </p>
        </Card>
      </Grid>
    </Grid>
  );
}

Login.propTypes = {
  isAuthenticated: PropTypes.bool,
  error: PropTypes.string,
  loading: PropTypes.bool,
  onLogin: PropTypes.func
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null,
    error: state.auth.error,
    loading: state.auth.loading
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onLogin: (email, password) => dispatch(actions.login(email, password)),
    onOpenRegisterModal: () => dispatch(actions.openRegisterModal())
  }
}

export default injectIntl(connect(mapStateToProps, mapDispatchToProps)(Login));