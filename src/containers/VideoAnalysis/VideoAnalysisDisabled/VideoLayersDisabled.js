import React from 'react';
import VideoLayerDisabled from './VideoLayerDisabled';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const VideoLayersDisabled = (props) => {
  return (
    <div style={{ 
      background: '#0E1420', 
      maxHeight: '396px', 
      overflow: 'auto',
      marginTop: '90px' }}>
      {props.videoLayers.map((layer, index) =>
        <VideoLayerDisabled
          key={layer.id}
          index={index}
          id={layer.id}
          labelColor={layer.marker_type.color}
          labelName={layer.marker_type.name}
          frameFrom={layer.frame_from}
          frameTo={layer.frame_to}
          sliderMax={props.sliderMax}/>)}
    </div>
  );
}

VideoLayersDisabled.propTypes = {
  videoLayers: PropTypes.array,
  sliderMax: PropTypes.number
}

const mapStateToProps = state => {
  return {
    videoLayers: state.videoAnalysis.videoLayers
  }
}

export default connect(mapStateToProps)(VideoLayersDisabled);