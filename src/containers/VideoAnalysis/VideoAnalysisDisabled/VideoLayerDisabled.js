import React, { useRef, useEffect } from 'react';
import { Grid, Slider } from '@material-ui/core';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PlayArrow from '@material-ui/icons/PlayArrow';
import * as actions from '../../../store/actions/index';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Comment from '@material-ui/icons/Comment';
import Pause from '@material-ui/icons/Pause';
import StyledTooltip from '../../../components/UI/StyledTooltip/StyledTooltip';
import { injectIntl } from 'react-intl';
import ButtonSecondarySmall from '../../../components/UI/Buttons/ButtonSecondary/ButtonSecondarySmall/ButtonSecondarySmall';

const VideoLayerDisabled = ({intl, ...props}) => {
  const sliderTimeLineRef = useRef();

  const texts = {
    play: intl.formatMessage({
      id: 'tools.play'
    }),
    pause: intl.formatMessage({
      id: 'tools.pause'
    }),
    addComment: intl.formatMessage({
      id: 'tools.addComment'
    }),
    visible: intl.formatMessage({
      id: 'tools.visible'
    }),
    unvisible: intl.formatMessage({
      id: 'tools.unvisible'
    })
  }

  const styleSliderLabel = (color, text) => {
    sliderTimeLineRef.current.style.color = color;
    const timeLineLabel = sliderTimeLineRef.current.childNodes[1];
    timeLineLabel.style.height = '40px';
    timeLineLabel.style.marginTop = '-20px';
    timeLineLabel.style.textAlign = 'center';
    timeLineLabel.innerHTML = '<span style="color: white">' + text + '</span>';
  }

  useEffect(() => {
    if (props.labelName && props.labelName) {
      const backgroundColor = props.labelColor.includes('#') ? props.labelColor : '#' + props.labelColor;
      styleSliderLabel(backgroundColor, props.labelName);
    }
  }, [props.labelColor, props.labelName]);

  const handleCommentsSwitchChange = () => {
    if (props.showCommentLayerId) {
      props.onSetShowCommentLayerId(null);
    } else {
      props.onSetShowCommentLayerId(props.id);
    }
  }

  let playPauseIcon = null;
  const isPlaying = props.playVideoLayer && props.playVideoLayer.index === props.index;
  if (isPlaying) {
      playPauseIcon = <StyledTooltip title={texts.pause}><Pause /></StyledTooltip>;
  } else {
    playPauseIcon = <StyledTooltip title={texts.play}><PlayArrow /></StyledTooltip>;
  }

  const playPauseButtonClickHandler = () => {
    if (props.playVideoLayer) {
      props.onPlayOnlyOneVideoLayer(null);
    } else {
      props.onPlayOnlyOneVideoLayer(props.index);
    }
  }

  return (
    <Grid container style={{ borderTop: '1px solid black', padding: '10px' }}>
      <Grid item xs={3}>
        <ButtonSecondarySmall 
          disabled={props.playVideoLayer && props.playVideoLayer.index !== props.index} 
          style={{minWidth: '5px', color: '#cccccc', marginLeft: '40px', marginRight: '10px'}}
          onClick={playPauseButtonClickHandler}>
          {playPauseIcon}
        </ButtonSecondarySmall>
        <StyledTooltip title={texts.addComment}>
          <ButtonSecondarySmall
            onClick={handleCommentsSwitchChange}
            style={{minWidth: '5px', color: '#cccccc', marginRight: '10px', marginLeft: '10px'}}>
            <Comment />
          </ButtonSecondarySmall>
        </StyledTooltip>
        <ButtonSecondarySmall 
          style={{minWidth: '5px', color: '#cccccc', marginLeft: '10px'}}
          onClick={() => {props.onSetVisibleLayer(props.index, !props.videoLayers[props.index].visible)}}>
          {props.videoLayers[props.index].visible ? 
            <StyledTooltip title={texts.visible}><Visibility /></StyledTooltip>
          : 
            <StyledTooltip title={texts.unvisible}><VisibilityOff /></StyledTooltip>}
        </ButtonSecondarySmall>
      </Grid>
      <Grid item xs={9}>
        <Slider
          ref={sliderTimeLineRef}
          style={{ marginTop: '15px', color: '#0E76BC' }}
          value={[props.frameFrom, props.frameTo]}
          onChange={() => { }}
          aria-labelledby="range-slider"
          max={props.sliderMax} />
      </Grid>
    </Grid>
  );
}

VideoLayerDisabled.propTypes = {
  sliderMax: PropTypes.number,
  index: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  frameFrom: PropTypes.number.isRequired,
  frameTo: PropTypes.number.isRequired,
  onSetShowCommentLayerId: PropTypes.func,
  onSetVisibleLayer: PropTypes.func,
  showCommentLayerId: PropTypes.number,
  videoLayers: PropTypes.array,
  onPlayOnlyOneVideoLayer: PropTypes.func,
  playVideoLayer: PropTypes.object,
  labelColor: PropTypes.string,
  labelName: PropTypes.string
}

const mapStateToProps = state => {
  return {
    videoLayers: state.videoAnalysis.videoLayers,
    showCommentLayerId: state.videoAnalysis.showCommentLayerId,
    playVideoLayer: state.videoAnalysis.playVideoLayer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onDeleteVideoLayer: (id) => dispatch(actions.deleteVideoLayer(id)),
    onSetVisibleLayer: (index, visible) => dispatch(actions.setVisibleLayer(index, visible)),
    onSetShowCommentLayerId: (id) => dispatch(actions.setShowCommentLayerId(id)),
    onPlayOnlyOneVideoLayer: (index) => dispatch(actions.playOnlyOneVideoLayer(index))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(VideoLayerDisabled));