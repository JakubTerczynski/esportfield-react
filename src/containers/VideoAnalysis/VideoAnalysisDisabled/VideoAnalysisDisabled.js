import React, { useState, useRef, useEffect } from 'react';
import { Container, Grid, Card } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import classes from '../VideoAnalysis.module.scss';
import VideoLayersDisabled from './VideoLayersDisabled';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../../../store/actions/index';
import VideoPlayer from '../VideoPlayer/VideoPlayer';
import CommentsModal from '../CommentsModal/CommentsModal';
import Spinner from '../../../components/UI/Spinner/Spinner';

const VideoAnalysisDisabled = ({ intl, match, ...props }) => {
  const [maxValueSlider, setMaxValueSlider] = useState(0);
  const [videoCurrentFrame, setVideoCurrentFrame] = useState(0);
  const [playVideo, setPlayVideo] = useState(false);
  const videoPlayerRef = useRef();
  const { onPlayOnlyOneVideoLayer,
    onSetDrawLayerProps,
    onSetShowCommentLayerId,
    onGetVideoLayers,
    onGetVideoAnalysis } = props;

  useEffect(() => {
    onPlayOnlyOneVideoLayer(null);
  }, [onPlayOnlyOneVideoLayer]);

  useEffect(() => {
    onSetDrawLayerProps(null, null);
  }, [onSetDrawLayerProps]);

  useEffect(() => {
    onSetShowCommentLayerId(null);
  }, [onSetShowCommentLayerId]);
  
  useEffect(() => {
    onGetVideoAnalysis(match.params.id);
  }, [onGetVideoAnalysis, match.params.id]);

  useEffect(() => {
    onGetVideoLayers(match.params.id);
  }, [onGetVideoLayers, match.params.id]);

  useEffect(() => {
    if (props.videoAnalysis) {
      videoPlayerRef.current.onloadeddata = () => setMaxValueSlider(videoPlayerRef.current.duration * 24);

      const updateProgress = () => {
        setVideoCurrentFrame(videoPlayerRef.current.currentTime * 24);
      }
      videoPlayerRef.current.addEventListener("timeupdate", updateProgress, false);
  
      const videoPlayerCurrentRef = videoPlayerRef.current;
      return () => {
        videoPlayerCurrentRef.removeEventListener("timeupdate", updateProgress, false);
      }
    }
  }, [videoPlayerRef, props.videoAnalysis]);

  const texts = {
    videoAnalysis: intl.formatMessage({
      id: 'video.analysis'
    }),
    summary: intl.formatMessage({
      id: 'common.summary'
    })
  }

  let layerImgs = null;
  if (props.videoLayers && videoPlayerRef.current) {
    layerImgs = props.videoLayers.map(layer => {
      if (layer.image.url && videoCurrentFrame >= layer.frame_from && videoCurrentFrame <= layer.frame_to && layer.visible) {
        return (<img
          key={layer.id}
          src={layer.image.url}
          alt={'imgLayer' + layer.id}
          onClick={() => setPlayVideo(!playVideo)}
          style={{ position: 'absolute' }} />)
      } else {
        return null;
      }
    });
  }

  let videoAnalysis = null;
  if (props.videoAnalysis) {
    videoAnalysis = <Container>
      <Grid container justify="center" style={{ margin: '60px 0px' }}>
        <Grid item xs={12}>
          <Card className={classes.Card}>
            <h1 className="header-text-bold font-24px">{texts.videoAnalysis}</h1>
            <p className="header-text-regular font-17px">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.
            </p>
            <br />
            <div style={{ position: 'relative', height: '580px' }}>
              <VideoPlayer
                ref={videoPlayerRef}
                src={props.videoAnalysis.game_video.video.url}
                maxValueSlider={maxValueSlider}
                videoCurrentFrame={videoCurrentFrame}
                playVideo={playVideo} />
              {layerImgs}
            </div>
            <br /><br />
            <VideoLayersDisabled sliderMax={maxValueSlider} />
            <CommentsModal disabledView={true} />
            <br />
            <h3 className="header-text-bold font-20px">{texts.summary}</h3>
            <p className="header-text-regular font-17px">{props.videoAnalysis.summary}</p>
          </Card>
        </Grid>
      </Grid>
    </Container>;
  } else {
    videoAnalysis = <Spinner />
  }

  return videoAnalysis;
}

VideoAnalysisDisabled.propTypes = {
  videoLayers: PropTypes.array,
  onPlayOnlyOneVideoLayer: PropTypes.func,
  onSetDrawLayerProps: PropTypes.func,
  onSetShowCommentLayerId: PropTypes.func,
  onGetVideoLayers: PropTypes.func,
  onGetVideoAnalysis: PropTypes.func,
  videoAnalysis: PropTypes.object
}

const mapStateToProps = state => {
  return {
    videoAnalysis: state.videoAnalysis.videoAnalysis,
    videoLayers: state.videoAnalysis.videoLayers
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onPlayOnlyOneVideoLayer: (index) => dispatch(actions.playOnlyOneVideoLayer(index)),
    onSetDrawLayerProps: (id, toolType) => dispatch(actions.setDrawLayerProps(id, toolType)),
    onSetShowCommentLayerId: (id) => dispatch(actions.setShowCommentLayerId(id)),
    onGetVideoLayers: (gameAnalysisId) => dispatch(actions.getVideoLayers(gameAnalysisId)),
    onGetVideoAnalysis: (id) => dispatch(actions.getVideoAnalysis(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(VideoAnalysisDisabled));