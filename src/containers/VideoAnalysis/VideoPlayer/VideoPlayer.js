import React, { forwardRef, useState, useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Pause from '@material-ui/icons/Pause';
import VolumeUp from '@material-ui/icons/VolumeUp';
import VolumeOff from '@material-ui/icons/VolumeOff';
import FastForward from '@material-ui/icons/FastForward';
import FastRewind from '@material-ui/icons/FastRewind';
import Fullscreen from '@material-ui/icons/Fullscreen';
import { IconButton, Grid, Slider } from '@material-ui/core';
import { formatTime } from '../../../shared/utility';
import * as actions from '../../../store/actions/index';

const VideoPlayer = forwardRef((props, ref) => {
  const [playIconVisible, setPlayIconVisible] = useState(true);
  const volumeSliderRef = useRef();
  const progressSliderRef = useRef();
  const playPauseButtonRef = useRef();
  const [volume, setVolume] = useState(0.5);
  const [volumeMuted, setVolumeMuted] = useState(false);
  const [visibleVolumeSlider, setVisibleVolumeSlider] = useState(false);
  const playVideoLayer = props.playVideoLayer;
  const onPlayOnlyOneVideoLayer = props.onPlayOnlyOneVideoLayer;

  useEffect(() => {
    if (ref.current && playVideoLayer) {
      const currentFrame = ref.current.currentTime * 24;
      if (currentFrame < playVideoLayer.frame_from || currentFrame > playVideoLayer.frame_to) {
        ref.current.currentTime = playVideoLayer.frame_from / 24;
      }
      ref.current.play();
    } else if (ref.current && !playVideoLayer) {
      ref.current.pause();
    }
  }, [ref, playVideoLayer, onPlayOnlyOneVideoLayer]);

  useEffect(() => {
    if (ref.current && playVideoLayer) {
      if (props.videoCurrentFrame >= playVideoLayer.frame_to) {
        ref.current.pause();
        onPlayOnlyOneVideoLayer(null);
      }
    }
  }, [ref, props.videoCurrentFrame, playVideoLayer, onPlayOnlyOneVideoLayer]);

  useEffect(() => {
    const sliderThumb = progressSliderRef.current.childNodes[3];
    sliderThumb.style.color = 'white';
    const heightLine = 180 + 90 * props.videoLayers.length;
    sliderThumb.innerHTML = `<div style="border: 0.5px solid white; height: ${heightLine}px; margin-top: ${heightLine}px; margin-left: 3px;"></div>;`;
  }, [props.videoLayers]);

  useEffect(() => {
    if (props.playVideo) {
      setPlayIconVisible(false);
      ref.current.play();
    } else {
      setPlayIconVisible(true);
      ref.current.pause();
    }
  }, [props.playVideo, ref]);

  const togglePlayPause = () => {
    props.onSetDrawLayerProps(null, null);
    if (ref.current.paused || ref.current.ended) {
      setPlayIconVisible(false);
      ref.current.play();
    } else {
      setPlayIconVisible(true);
      ref.current.pause();
    }
  }

  const handleSliderprogressChange = (event, newValue) => {
    ref.current.currentTime = newValue / 24;
  };

  const handleSilderVolumeChange = (event, newValue) => {
    setVolume(newValue);
    ref.current.volume = newValue;
  }

  const toggleVolumeMuted = () => {
    ref.current.muted = !ref.current.muted;
    setVolumeMuted(!volumeMuted);
  }

  const setVideoSpeedSlower = () => {
    if (ref.current.playbackRate >= 0.25 && ref.current.playbackRate <= 1) {
      ref.current.playbackRate *= 0.5;
    } else {
      ref.current.playbackRate = 1;
    }
  }

  const setVideoSpeedFaster = () => {
    if (ref.current.playbackRate >= 1 && ref.current.playbackRate <= 2.25) {
      ref.current.playbackRate *= 1.5;
    } else {
      ref.current.playbackRate = 1;
    }
  }

  const toggleFullscreen = () => {
    if (ref.current.requestFullScreen) {
      ref.current.requestFullScreen();
    } else if (ref.current.webkitRequestFullScreen) {
      ref.current.webkitRequestFullScreen();
    } else if (ref.current.mozRequestFullScreen) {
      ref.current.mozRequestFullScreen();
    }
  }

  return (
    <div style={{ position: 'absolute', width: '100%', background: 'rgb(14, 20, 32)', borderBottom: '1px solid black' }}>
      <video
        ref={ref}
        onClick={togglePlayPause}
        style={{ width: '100%' }}>
        <source src={props.src} type="video/mp4" />
      </video>
      <div id="controls" style={{ height: '60px' }}>
        <Grid container style={{ padding: '10px' }}>
          <Grid item xs={3}>
            <Grid container>
              <Grid item>
                <IconButton
                  style={{ padding: '8px', color: '#0E76BC' }}
                  onClick={setVideoSpeedSlower}>
                  <FastRewind />
                </IconButton>
                <IconButton
                  ref={playPauseButtonRef}
                  style={{ padding: '8px', color: '#0E76BC', opacity: Boolean(props.playVideoLayer) ? 0.25 : 1 }}
                  disabled={Boolean(props.playVideoLayer)}
                  onClick={togglePlayPause}>
                  {playIconVisible ? <PlayArrow /> : <Pause />}
                </IconButton>
                <IconButton
                  style={{ padding: '8px', color: '#0E76BC' }}
                  onClick={setVideoSpeedFaster}>
                  <FastForward />
                </IconButton>
                <IconButton style={{ padding: '8px', color: '#0E76BC' }}
                  onClick={toggleFullscreen}>
                  <Fullscreen />
                </IconButton>
              </Grid>
              <Grid item style={{ position: 'relative' }}>
                <IconButton style={{ padding: '8px', color: '#0E76BC' }}
                  onClick={toggleVolumeMuted}
                  onMouseEnter={() => { setVisibleVolumeSlider(true) }}
                  onMouseLeave={() => { setVisibleVolumeSlider(false) }}>
                  {volumeMuted ? <VolumeOff /> : <VolumeUp />}
                </IconButton>
                {visibleVolumeSlider && (<Slider
                  ref={volumeSliderRef}
                  value={volume}
                  onChange={handleSilderVolumeChange}
                  orientation="vertical"
                  min={0} max={1} step={0.1}
                  disabled={volumeMuted}
                  onMouseEnter={() => { setVisibleVolumeSlider(true) }}
                  onMouseLeave={() => { setVisibleVolumeSlider(false) }}
                  style={{ height: '100px', position: 'absolute', bottom: '40px', right: '10px', background: 'rgb(192,192,192,0.5)', borderRadius: '5px' }} />)}
              </Grid>
              <Grid item>
                <div style={{ padding: '10px 0px', color: 'white' }}>{formatTime(props.videoCurrentFrame / 24)}</div>
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={9}>
            <Slider
              ref={progressSliderRef}
              style={{ marginTop: '10px', color: '#0E76BC' }}
              value={props.videoCurrentFrame}
              onChange={(handleSliderprogressChange)}
              step={0.01}
              aria-labelledby="range-slider"
              max={props.maxValueSlider} />
          </Grid>
        </Grid>
      </div>
    </div>
  );
})

VideoPlayer.propTypes = {
  src: PropTypes.string.isRequired,
  maxValueSlider: PropTypes.number,
  videoCurrentFrame: PropTypes.number,
  playVideoLayer: PropTypes.object,
  onPlayOnlyOneVideoLayer: PropTypes.func,
  onSetDrawLayerProps: PropTypes.func,
  videoLayers: PropTypes.array,
  playVideo: PropTypes.bool
}

const mapStateToProps = state => {
  return {
    videoLayers: state.videoAnalysis.videoLayers,
    playVideoLayer: state.videoAnalysis.playVideoLayer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onPlayOnlyOneVideoLayer: (index) => dispatch(actions.playOnlyOneVideoLayer(index)),
    onSetDrawLayerProps: (id, toolType) => dispatch(actions.setDrawLayerProps(id, toolType))
  }
}

export default connect(mapStateToProps, mapDispatchToProps, null, { forwardRef: true })(VideoPlayer);