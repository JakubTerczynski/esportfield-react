import React, { useState, useRef, useEffect } from 'react';
import { Container, Grid, Card, Slider } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import classes from './VideoAnalysis.module.scss';
import ButtonSecondary from '../../components/UI/Buttons/ButtonSecondary/ButtonSecondary';
import VideoLayers from './VideoLayers/VideoLayers';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../../store/actions/index';
import VideoPlayer from './VideoPlayer/VideoPlayer';
import CommentsModal from './CommentsModal/CommentsModal';
import VideoLayerPainter from './VideoLayerPainter/VideoLayerPainter';
import SummaryForm from './SummaryForm/SummaryForm';
import Spinner from '../../components/UI/Spinner/Spinner';

const VideoAnalysis = ({ intl, match, ...props }) => {
  const [[frameFrom, frameTo], setFrameRange] = useState([0, 0]);
  const [maxValueSlider, setMaxValueSlider] = useState(0);
  const [videoCurrentFrame, setVideoCurrentFrame] = useState(0);
  const [playVideo, setPlayVideo] = useState(false);
  const videoPlayerRef = useRef();
  const { onGetMarkerTypes,
    onPlayOnlyOneVideoLayer,
    onSetDrawLayerProps,
    onSetShowCommentLayerId,
    onGetVideoLayers,
    onGetVideoAnalysis } = props;

  useEffect(() => {
    onPlayOnlyOneVideoLayer(null);
  }, [onPlayOnlyOneVideoLayer]);

  useEffect(() => {
    onSetDrawLayerProps(null, null);
  }, [onSetDrawLayerProps]);

  useEffect(() => {
    onSetShowCommentLayerId(null);
  }, [onSetShowCommentLayerId]);
  
  useEffect(() => {
    onGetVideoAnalysis(match.params.id);
  }, [onGetVideoAnalysis, match.params.id]);

  useEffect(() => {
    onGetVideoLayers(match.params.id);
  }, [onGetVideoLayers, match.params.id]);

  useEffect(() => {
    onGetMarkerTypes();
  }, [onGetMarkerTypes]);

  useEffect(() => {
    if (props.videoAnalysis) {
      videoPlayerRef.current.onloadeddata = () => setMaxValueSlider(videoPlayerRef.current.duration * 24);

      const updateProgress = () => {
        setVideoCurrentFrame(videoPlayerRef.current.currentTime * 24);
      }
      videoPlayerRef.current.addEventListener("timeupdate", updateProgress, false);
  
      const videoPlayerCurrentRef = videoPlayerRef.current;
      return () => {
        videoPlayerCurrentRef.removeEventListener("timeupdate", updateProgress, false);
      }
    }
  }, [videoPlayerRef, props.videoAnalysis]);

  const texts = {
    videoAnalysis: intl.formatMessage({
      id: 'video.analysis'
    }),
    addNewLayer: intl.formatMessage({
      id: 'video.addNewLayer'
    }),
    tools: intl.formatMessage({
      id: 'tools'
    }),
    cancel: intl.formatMessage({
      id: 'common.cancel'
    }),
    addStartMarker: intl.formatMessage({
      id: 'video.addStartMarker'
    }),
    addStopMarker: intl.formatMessage({
      id: 'video.addStopMarker'
    })
  }

  const handleFrameFromChange = () => {
    const currentFrame = videoPlayerRef.current.currentTime * 24;
    setFrameRange([currentFrame, frameTo]);
  };

  const handleFrameToChange = () => {
    const currentFrame = videoPlayerRef.current.currentTime * 24;
    setFrameRange([frameFrom, currentFrame]);
  };

  const addVideoLayerHandler = () => {
    props.onCreateVideoLayer(match.params.id, frameFrom, frameTo);
    videoPlayerRef.current.currentTime = frameFrom / 24;
  }

  let layerImgs = null;
  if (props.videoLayers && videoPlayerRef.current) {
    layerImgs = props.videoLayers.map(layer => {
      if (layer.image.url && videoCurrentFrame >= layer.frame_from && videoCurrentFrame <= layer.frame_to && layer.visible) {
        return (<img
          key={layer.id}
          src={layer.image.url}
          alt={'imgLayer' + layer.id}
          onClick={() => setPlayVideo(!playVideo)}
          style={{ position: 'absolute' }} />)
      } else {
        return null;
      }
    });
  }

  let videoLayerPainter = null;
  if (props.drawLayerId !== null) {
    const drawLayer = props.videoLayers.find(l => l.id === props.drawLayerId);
    if (videoCurrentFrame >= drawLayer.frame_from && videoCurrentFrame <= drawLayer.frame_to && drawLayer.visible) {
      videoLayerPainter = <VideoLayerPainter />
    }
  }

  let videoAnalysis = null;
  if (props.videoAnalysis) {
    videoAnalysis = <Container>
      <Grid container justify="center" style={{ margin: '60px 0px' }}>
        <Grid item xs={12}>
          <Card className={classes.Card}>
            <h1 className="header-text-bold font-24px">{texts.videoAnalysis}</h1>
            <p className="header-text-regular font-17px">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar sic tempor. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus pronin sapien nunc accuan eget.
            </p>
            <br />
            <div style={{ position: 'relative', height: '580px' }}>
              <VideoPlayer
                ref={videoPlayerRef}
                src={props.videoAnalysis.game_video.video.url}
                maxValueSlider={maxValueSlider}
                videoCurrentFrame={videoCurrentFrame}
                playVideo={playVideo} />
              {props.drawLayerId ? videoLayerPainter : layerImgs}
            </div>
            <br /><br />
            <Grid container
              style={{ background: '#0E1420', padding: '10px', marginTop: '90px' }}>
              <Grid item xs={12}>
                <ButtonSecondary outline="true"
                  onClick={handleFrameFromChange}>{texts.addStartMarker}</ButtonSecondary>&nbsp;
                <ButtonSecondary outline="true"
                  onClick={handleFrameToChange}>{texts.addStopMarker}</ButtonSecondary><br />
              </Grid>
              <Grid item xs={3} className={classes.Tools}>
                <ButtonSecondary
                  onClick={() => { addVideoLayerHandler() }}
                  disabled={frameFrom === 0 || frameTo === 0 || frameFrom > frameTo}
                  style={{ marginTop: '10px' }}>
                  {texts.addNewLayer}
                </ButtonSecondary>
              </Grid>
              <Grid item xs={9}>
                <Slider
                  style={{ marginTop: '20px', color: '#0E76BC' }}
                  value={[frameFrom, frameTo]}
                  onChange={() => { }}
                  aria-labelledby="range-slider"
                  max={maxValueSlider} />
              </Grid>
            </Grid>
            <VideoLayers sliderMax={maxValueSlider} />
            <CommentsModal />
            <br />
            <SummaryForm 
              videoAnalysisId={props.videoAnalysis.id}
              videoAnalysisSummary={props.videoAnalysis.summary} />
          </Card>
        </Grid>
      </Grid>
    </Container>;
  } else {
    videoAnalysis = <Spinner />
  }

  return videoAnalysis;
}

VideoAnalysis.propTypes = {
  videoLayers: PropTypes.array,
  onCreateVideoLayer: PropTypes.func,
  onGetMarkerTypes: PropTypes.func,
  onPlayOnlyOneVideoLayer: PropTypes.func,
  onSetDrawLayerProps: PropTypes.func,
  onSetShowCommentLayerId: PropTypes.func,
  onGetVideoLayers: PropTypes.func,
  onGetVideoAnalysis: PropTypes.func,
  drawLayerId: PropTypes.number,
  playVideoLayer: PropTypes.object,
  videoAnalysis: PropTypes.object
}

const mapStateToProps = state => {
  return {
    videoAnalysis: state.videoAnalysis.videoAnalysis,
    videoLayers: state.videoAnalysis.videoLayers,
    drawLayerId: state.videoAnalysis.drawLayerId,
    playVideoLayer: state.videoAnalysis.playVideoLayer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onCreateVideoLayer: (gameAnalysisId, frameFrom, frameTo) => dispatch(actions.createVideoLayer(gameAnalysisId, frameFrom, frameTo)),
    onGetMarkerTypes: () => dispatch(actions.getMarkerTypes()),
    onPlayOnlyOneVideoLayer: (index) => dispatch(actions.playOnlyOneVideoLayer(index)),
    onSetDrawLayerProps: (id, toolType) => dispatch(actions.setDrawLayerProps(id, toolType)),
    onSetShowCommentLayerId: (id) => dispatch(actions.setShowCommentLayerId(id)),
    onGetVideoLayers: (gameAnalysisId) => dispatch(actions.getVideoLayers(gameAnalysisId)),
    onGetVideoAnalysis: (id) => dispatch(actions.getVideoAnalysis(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(VideoAnalysis));