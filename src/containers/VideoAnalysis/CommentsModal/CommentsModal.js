import React, { useState } from 'react';
import { Slide, Card, Grid } from '@material-ui/core';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import StyledInput from '../../../components/UI/StyledInput/StyledInput';
import { injectIntl } from 'react-intl';
import ButtonSecondarySmall from '../../../components/UI/Buttons/ButtonSecondary/ButtonSecondarySmall/ButtonSecondarySmall';
import ButtonSecondary from '../../../components/UI/Buttons/ButtonSecondary/ButtonSecondary';
import ArrowForward from '@material-ui/icons/ArrowForward';
import AccountCircle from '@material-ui/icons/AccountCircle';
import * as actions from '../../../store/actions/index';

const CommentsModal = ({ intl, ...props }) => {
  const [newComment, setNewComment] = useState('');

  const texts = {
    save: intl.formatMessage({
      id: 'common.save'
    }),
    cancel: intl.formatMessage({
      id: 'common.cancel'
    }),
    comment: intl.formatMessage({
      id: 'common.comment'
    }),
    noComments: intl.formatMessage({
      id: 'common.noComments'
    })
  };

  const newCommentChangeHandler = (event) => {
    setNewComment(event.target.value);
  }

  const closeModalHandle = () => {
    props.onSetShowCommentLayerId(null);
  }

  const setCommentOnLayer = () => {
    props.onSetCommentOnLayer(props.showCommentLayerId, newComment);
  }

  let commentText = null;
  if (props.showCommentLayerId && props.videoLayers.find(l => l.id === props.showCommentLayerId).comment) {
    commentText = props.videoLayers.find(l => l.id === props.showCommentLayerId).comment;
  }

  return (
    <Slide
      direction="left"
      in={Boolean(props.showCommentLayerId)}
      mountOnEnter unmountOnExit
      timeout={1000}>
      <Card style={{ padding: '20px', position: 'absolute', width: '400px', top: '500px', right: '50px' }}>
        <ButtonSecondarySmall
          outline="true"
          onClick={closeModalHandle}
          style={{ padding: 0 }}>
          <ArrowForward />
        </ButtonSecondarySmall>
        {!props.disabledView && <React.Fragment>
          <form>
            <StyledInput
              label={texts.comment}
              type="text"
              value={newComment}
              onChange={newCommentChangeHandler}
              multiline={true}
              rows={5} />
          </form>
          <Grid container>
            <Grid item xs={6}>
              <ButtonSecondarySmall
                outline="true"
                onClick={closeModalHandle}
                style={{ paddingTop: '10px' }}>
                {texts.cancel}
              </ButtonSecondarySmall>
            </Grid>
            <Grid item xs={6} style={{ textAlign: 'right' }}>
              <ButtonSecondary onClick={setCommentOnLayer}>
                {texts.save}
              </ButtonSecondary>
            </Grid>
          </Grid>
        </React.Fragment>}
        {commentText ? 
          <React.Fragment>
            {!props.disabledView && <hr style={{ border: '0.5px solid #5F5F5F', margin: '20px 0px' }} />}
            <div>
              <AccountCircle style={{ width: '37px', height: '37px', marginBottom: '-15px', paddingRight: '5px' }} />
              <span>
                <span style={{ color: '#0E76BC', cursor: 'pointer' }}>TRENER</span> 04.09.2019
              </span>
              <div style={{ padding: '10px', fontSize: '12px' }}>
                {commentText}
              </div>
            </div>
          </React.Fragment> : (props.disabledView ? <div>{texts.noComments}</div> : null)}
      </Card>
    </Slide>
  );
}

CommentsModal.propTypes = {
  videoLayers: PropTypes.array,
  showCommentLayerId: PropTypes.number,
  onSetShowCommentLayerId: PropTypes.func,
  onSetCommentOnLayer: PropTypes.func,
  disabledView: PropTypes.bool
}

const mapStateToProps = state => {
  return {
    videoLayers: state.videoAnalysis.videoLayers,
    showCommentLayerId: state.videoAnalysis.showCommentLayerId
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSetShowCommentLayerId: (id) => dispatch(actions.setShowCommentLayerId(id)),
    onSetCommentOnLayer: (indexLayer, comment) => dispatch(actions.setCommentOnLayer(indexLayer, comment))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(CommentsModal));