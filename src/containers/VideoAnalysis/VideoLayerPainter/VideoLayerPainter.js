import React, { useEffect, useRef } from 'react';
import { SketchField } from 'react-sketch';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../../../store/actions/index';
import { toDataUrl } from '../../../shared/utility';

const VideoLayerPainter = (props) => {
  const painterRef = useRef();
  const drawLayerId = props.drawLayerId;
  const onSetImageOnLayer = props.onSetImageOnLayer;
  const onSetDrawLayerProps = props.onSetDrawLayerProps;

  useEffect(() => {
    const painterRefCurrent = painterRef.current;
    return () => {
      onSetImageOnLayer(drawLayerId, painterRefCurrent.toDataURL());
      onSetDrawLayerProps(null, null);
    }
  }, [drawLayerId, onSetImageOnLayer, onSetDrawLayerProps, painterRef]);

  useEffect(() => {
    const imgUrl = props.videoLayers.find(layer => layer.id === drawLayerId).image.url;
    toDataUrl(imgUrl, (response) => painterRef.current.setBackgroundFromDataUrl(response));
  }, [props.videoLayers, drawLayerId]);

  return (
    <SketchField
      ref={painterRef}
      height='640px'
      tool={props.drawToolType.type}
      lineColor='red'
      lineWidth={props.drawToolType.lineWidth}
      backgroundColor='transparent' />
  );
}

VideoLayerPainter.propTypes = {
  videoLayers: PropTypes.array,
  drawLayerId: PropTypes.number.isRequired,
  drawToolType: PropTypes.object.isRequired,
  onSetImageOnLayer: PropTypes.func,
  onSetDrawLayerProps: PropTypes.func
}

const mapStateToProps = state => {
  return {
    videoLayers: state.videoAnalysis.videoLayers,
    drawLayerId: state.videoAnalysis.drawLayerId,
    drawToolType: state.videoAnalysis.drawToolType
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSetImageOnLayer: (layerId, image) => dispatch(actions.setImageOnLayer(layerId, image)),
    onSetDrawLayerProps: (id, toolType) => dispatch(actions.setDrawLayerProps(id, toolType)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoLayerPainter);