import React, { useState } from 'react';
import { injectIntl } from 'react-intl';
import ButtonSecondarySmall from '../../../../../components/UI/Buttons/ButtonSecondary/ButtonSecondarySmall/ButtonSecondarySmall';
import Edit from '@material-ui/icons/Edit';
import Brush from '@material-ui/icons/Brush';
import PanoramaFishEye from '@material-ui/icons/PanoramaFishEye';
import CropDin from '@material-ui/icons/CropDin';
import { Popover, Card } from '@material-ui/core';
import PropTypes from 'prop-types';
import classes from './PaintToolButton.module.scss';
import { connect } from 'react-redux';
import * as actions from '../../../../../store/actions/index';
import { VideoToolTypes } from '../../../../../shared/dicts/videoToolTypes';

const PaintToolButton = ({intl, ...props}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const openModal = Boolean(anchorEl);
  const modalId = openModal ? 'simple-popover' : undefined;

  const texts = {
    pencil: intl.formatMessage({
      id: 'common.pencil'
    }),
    brush: intl.formatMessage({
      id: 'common.brush'
    }),
    circle: intl.formatMessage({
      id: 'common.circle'
    }),
    rectangle: intl.formatMessage({
      id: 'common.rectangle'
    })
  }

  const openPaintToolMenu = (event) => {
    setAnchorEl(event.currentTarget);
  }

  const closePaintToolMenu = () => {
    setAnchorEl(null);
  }

  const choosePaintToolType = (toolType) => {
    props.onSetDrawLayerProps(props.layerId, toolType);
    closePaintToolMenu();
  }

  return (
    <React.Fragment>
      <ButtonSecondarySmall
        style={{ minWidth: '5px', color: '#cccccc', opacity: props.disabled ? 0.25 : 1 }}
        aria-describedby={modalId}
        onClick={openPaintToolMenu}
        disabled={props.disabled}>
        <Brush />
      </ButtonSecondarySmall>
      <Popover
        id={modalId}
        open={openModal}
        anchorEl={anchorEl}
        onClose={closePaintToolMenu}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}>
          <Card>
            <div 
              onClick={() => {choosePaintToolType(VideoToolTypes.pencil)}}
              className={classes.MenuItem}>
              <Edit /> {texts.pencil}
            </div>
            <div 
              onClick={() => {choosePaintToolType(VideoToolTypes.brush)}}
              className={classes.MenuItem}>
              <Brush /> {texts.brush}
            </div>
            <div 
              onClick={() => {choosePaintToolType(VideoToolTypes.circle)}}
              className={classes.MenuItem}>
              <PanoramaFishEye /> {texts.circle}
            </div>
            <div 
              onClick={() => {choosePaintToolType(VideoToolTypes.rectangle)}}
              className={classes.MenuItem}>
              <CropDin /> {texts.rectangle}
            </div>
          </Card>
        </Popover>
    </React.Fragment>
  );
}

PaintToolButton.propTypes = {
  disabled: PropTypes.bool,
  layerId: PropTypes.number,
  onSetDrawLayerProps: PropTypes.func
}
 
const mapDispatchToProps = dispatch => {
  return {
    onSetDrawLayerProps: (id, toolType) => dispatch(actions.setDrawLayerProps(id, toolType))
  }
}

export default connect(null, mapDispatchToProps)(injectIntl(PaintToolButton));