import React, { useRef, useState, useEffect } from 'react';
import { Grid, Slider } from '@material-ui/core';
import ButtonSecondarySmall from '../../../../components/UI/Buttons/ButtonSecondary/ButtonSecondarySmall/ButtonSecondarySmall';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import PlayArrow from '@material-ui/icons/PlayArrow';
import * as actions from '../../../../store/actions/index';
import MarkersModalButton from '../../MarkersModalButton/MarkersModalButton';
import Lock from '@material-ui/icons/Lock';
import LockOpen from '@material-ui/icons/LockOpen';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Comment from '@material-ui/icons/Comment';
import Pause from '@material-ui/icons/Pause';
import PaintToolButton from './PaintToolButton/PaintToolButton';
import StyledTooltip from '../../../../components/UI/StyledTooltip/StyledTooltip';
import { injectIntl } from 'react-intl';

const VideoLayer = ({intl, ...props}) => {
  const [disabledLayer, setDisabledLayer] = useState(false);
  const sliderTimeLineRef = useRef();

  const texts = {
    paint: intl.formatMessage({
      id: 'tools.paint'
    }),
    play: intl.formatMessage({
      id: 'tools.play'
    }),
    pause: intl.formatMessage({
      id: 'tools.pause'
    }),
    addLabel: intl.formatMessage({
      id: 'tools.addLabel'
    }),
    addComment: intl.formatMessage({
      id: 'tools.addComment'
    }),
    lock: intl.formatMessage({
      id: 'tools.lock'
    }),
    unlock: intl.formatMessage({
      id: 'tools.unlock'
    }),
    visible: intl.formatMessage({
      id: 'tools.visible'
    }),
    unvisible: intl.formatMessage({
      id: 'tools.unvisible'
    }),
    delete: intl.formatMessage({
      id: 'tools.delete'
    })
  }

  const styleSliderLabel = (color, text) => {
    sliderTimeLineRef.current.style.color = color;
    const timeLineLabel = sliderTimeLineRef.current.childNodes[1];
    timeLineLabel.style.height = '40px';
    timeLineLabel.style.marginTop = '-20px';
    timeLineLabel.style.textAlign = 'center';
    timeLineLabel.innerHTML = '<span style="color: white">' + text + '</span>';
  }

  useEffect(() => {
    if (props.labelName && props.labelName) {
      const backgroundColor = props.labelColor.includes('#') ? props.labelColor : '#' + props.labelColor;
      styleSliderLabel(backgroundColor, props.labelName);
    }
  }, [props.labelColor, props.labelName]);

  const handleCommentsSwitchChange = () => {
    if (props.showCommentLayerId) {
      props.onSetShowCommentLayerId(null);
    } else {
      props.onSetShowCommentLayerId(props.id);
    }
  }

  let playPauseIcon = null;
  const isPlaying = props.playVideoLayer && props.playVideoLayer.index === props.index;
  if (isPlaying) {
      playPauseIcon = <StyledTooltip title={texts.pause}><Pause /></StyledTooltip>;
  } else {
    playPauseIcon = <StyledTooltip title={texts.play}><PlayArrow /></StyledTooltip>;
  }

  const playPauseButtonClickHandler = () => {
    if (props.playVideoLayer) {
      props.onPlayOnlyOneVideoLayer(null);
    } else {
      props.onPlayOnlyOneVideoLayer(props.index);
    }
  }

  return (
    <Grid container style={{ borderTop: '1px solid black', padding: '10px' }}>
      <Grid item xs={3}>
        <StyledTooltip title={texts.paint}>
          <PaintToolButton disabled={disabledLayer} layerId={props.id} />
        </StyledTooltip>
        <ButtonSecondarySmall 
          disabled={disabledLayer || (props.playVideoLayer && props.playVideoLayer.index !== props.index)} 
          style={{minWidth: '5px', color: '#cccccc', opacity: disabledLayer ? 0.25 : 1}}
          onClick={playPauseButtonClickHandler}>
          {playPauseIcon}
        </ButtonSecondarySmall>
        <StyledTooltip title={texts.addLabel}>
          <MarkersModalButton
            disabled={disabledLayer}
            onAddMarkerType={styleSliderLabel}
            layerId={props.id} />
        </StyledTooltip>
        <StyledTooltip title={texts.addComment}>
          <ButtonSecondarySmall
            disabled={disabledLayer}
            onClick={handleCommentsSwitchChange}
            style={{minWidth: '5px', color: '#cccccc', opacity: disabledLayer ? 0.25 : 1}}>
            <Comment />
          </ButtonSecondarySmall>
        </StyledTooltip>
        <ButtonSecondarySmall 
          style={{minWidth: '5px', color: '#cccccc'}}
          onClick={() => setDisabledLayer(!disabledLayer)}>
          {disabledLayer ? 
            <StyledTooltip title={texts.unlock}><LockOpen /></StyledTooltip> 
          : 
            <StyledTooltip title={texts.lock}><Lock /></StyledTooltip>}
        </ButtonSecondarySmall>
        <ButtonSecondarySmall 
          disabled={disabledLayer} 
          style={{minWidth: '5px', color: '#cccccc', opacity: disabledLayer ? 0.25 : 1}}
          onClick={() => {props.onSetVisibleLayer(props.index, !props.videoLayers[props.index].visible)}}>
          {props.videoLayers[props.index].visible ? 
            <StyledTooltip title={texts.visible}><Visibility /></StyledTooltip>
          : 
            <StyledTooltip title={texts.unvisible}><VisibilityOff /></StyledTooltip>}
        </ButtonSecondarySmall>
        <StyledTooltip title={texts.delete}>
          <ButtonSecondarySmall
            disabled={disabledLayer}
            onClick={() => { props.onDeleteVideoLayer(props.id) }}
            style={{minWidth: '5px', color: '#cccccc', opacity: disabledLayer ? 0.25 : 1}}>
            <DeleteOutline />
          </ButtonSecondarySmall>
        </StyledTooltip>
      </Grid>
      <Grid item xs={9}>
        <Slider
          ref={sliderTimeLineRef}
          style={{ marginTop: '15px', color: '#0E76BC', opacity: disabledLayer ? 0.25 : 1 }}
          value={[props.frameFrom, props.frameTo]}
          onChange={() => { }}
          aria-labelledby="range-slider"
          max={props.sliderMax}
          disabled={disabledLayer} />
      </Grid>
    </Grid>
  );
}

VideoLayer.propTypes = {
  sliderMax: PropTypes.number,
  index: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  frameFrom: PropTypes.number.isRequired,
  frameTo: PropTypes.number.isRequired,
  onDeleteVideoLayer: PropTypes.func,
  onSetShowCommentLayerId: PropTypes.func,
  onSetVisibleLayer: PropTypes.func,
  showCommentLayerId: PropTypes.number,
  videoLayers: PropTypes.array,
  onPlayOnlyOneVideoLayer: PropTypes.func,
  playVideoLayer: PropTypes.object,
  labelColor: PropTypes.string,
  labelName: PropTypes.string
}

const mapStateToProps = state => {
  return {
    videoLayers: state.videoAnalysis.videoLayers,
    showCommentLayerId: state.videoAnalysis.showCommentLayerId,
    playVideoLayer: state.videoAnalysis.playVideoLayer
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onDeleteVideoLayer: (id) => dispatch(actions.deleteVideoLayer(id)),
    onSetVisibleLayer: (index, visible) => dispatch(actions.setVisibleLayer(index, visible)),
    onSetShowCommentLayerId: (id) => dispatch(actions.setShowCommentLayerId(id)),
    onPlayOnlyOneVideoLayer: (index) => dispatch(actions.playOnlyOneVideoLayer(index))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(VideoLayer));