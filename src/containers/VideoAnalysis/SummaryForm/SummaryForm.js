import React, { useState } from 'react';
import StyledInput from '../../../components/UI/StyledInput/StyledInput';
import { Grid } from '@material-ui/core';
import ButtonSecondarySmall from '../../../components/UI/Buttons/ButtonSecondary/ButtonSecondarySmall/ButtonSecondarySmall';
import ButtonSecondary from '../../../components/UI/Buttons/ButtonSecondary/ButtonSecondary';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as actions from '../../../store/actions/index';

const SummaryForm = ({intl, ...props}) => {
  const [summary, setSummary] = useState(props.videoAnalysisSummary);

  const texts = {
    save: intl.formatMessage({
      id: 'common.save'
    }),
    saveAndSend: intl.formatMessage({
      id: 'common.saveAndSend'
    }),
    summary: intl.formatMessage({
      id: 'common.summary'
    }),
    reset: intl.formatMessage({
      id: 'common.reset'
    })
  }

  const summaryChangeHandler = (event) => {
    setSummary(event.target.value);
  }

  return (
    <React.Fragment>
      <form>
        <StyledInput
          label={texts.summary}
          type="text"
          value={summary}
          onChange={summaryChangeHandler}
          multiline={true}
          rows={8} />
      </form>
      <Grid container>
        <Grid item xs={6}>
          <ButtonSecondarySmall 
            outline="true" 
            style={{ paddingTop: '10px' }}
            onClick={() => setSummary('')}>
            {texts.reset}
          </ButtonSecondarySmall>
        </Grid>
        <Grid item xs={6} style={{ textAlign: 'right' }}>
          <ButtonSecondary
            onClick={() => props.onUpdateVideoAnalysis(props.videoAnalysisId, summary, '')}>
            {texts.save}
          </ButtonSecondary>&nbsp;
          <ButtonSecondary
            onClick={() => props.onUpdateVideoAnalysis(props.videoAnalysisId, summary, '')}>
            {texts.saveAndSend}
          </ButtonSecondary>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

SummaryForm.propTypes = {
  onUpdateVideoAnalysis: PropTypes.func,
  videoAnalysisId: PropTypes.number.isRequired,
  videoAnalysisSummary: PropTypes.string
}

const mapDispatchToProps = dispatch => {
  return {
    onUpdateVideoAnalysis: (id, summary, recommendation) => dispatch(actions.updateVideoAnalysis(id, summary, recommendation))
  }
}

export default connect(null, mapDispatchToProps)(injectIntl(SummaryForm));