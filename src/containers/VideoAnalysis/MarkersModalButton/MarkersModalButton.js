import React, { useState } from 'react';
import { Popover, Card, Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import ButtonSecondarySmall from '../../../components/UI/Buttons/ButtonSecondary/ButtonSecondarySmall/ButtonSecondarySmall';
import { CirclePicker } from 'react-color';
import './ColorPicker.scss';
import { injectIntl } from 'react-intl';
import ButtonSecondary from '../../../components/UI/Buttons/ButtonSecondary/ButtonSecondary';
import Search from '@material-ui/icons/Search';
import Close from '@material-ui/icons/Close';
import StyledInput from '../../../components/UI/StyledInput/StyledInput';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import ListAlt from '@material-ui/icons/ListAlt';
import DeleteOutline from '@material-ui/icons/DeleteOutline';

const MarkersModalButton = ({ intl, ...props }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [markerColor, setMarkerColor] = useState('#00bcd4');
  const [markerText, setMarkerText] = useState('');
  const [visibleSearchArea, setVisibleSearchArea] = useState(false);
  const openModal = Boolean(anchorEl);
  const modalId = openModal ? 'simple-popover' : undefined;

  const texts = {
    save: intl.formatMessage({
      id: 'common.save'
    }),
    cancel: intl.formatMessage({
      id: 'common.cancel'
    }),
    addMarker: intl.formatMessage({
      id: 'markers.add'
    }),
    markerName: intl.formatMessage({
      id: 'markers.name'
    }),
    searchMarker: intl.formatMessage({
      id: 'markers.search'
    })
  };

  const handleOpenButtonClick = (event) => {
    setAnchorEl(event.currentTarget);
  }

  const handleMarkersModalClose = () => {
    setAnchorEl(null);
  }

  const markerTextChangeHandler = (event) => {
    setMarkerText(event.target.value);
  }

  const createMarkerType = () => {
    if (markerText !== '') {
      props.onCreateMarkerType(markerText, markerColor, props.layerId);
      props.onAddMarkerType(markerColor, markerText);
      handleMarkersModalClose();
    }
  }

  const selectMarkerType = (color, text, id) => {
    props.onAddMarkerType(color, text);
    handleMarkersModalClose();
    props.onSetMarkerTypeOnLayer(props.layerId, id, color, text);
  }

  let markerTypesLabels = null;
  if (props.markerTypes) {
    markerTypesLabels = props.markerTypes.map(markerType => {
      const backgroundColor = markerType.color.includes('#') ? markerType.color : '#' + markerType.color;
      return <div key={markerType.id} style={{ display: 'flex' }}>
        <div
          style={{
            background: backgroundColor,
            padding: '10px',
            margin: '10px 0px',
            borderRadius: '10px',
            cursor: 'pointer',
            color: 'white',
            flexGrow: 1
          }}
          onClick={() => selectMarkerType(backgroundColor, markerType.name, markerType.id)}>
          {markerType.name}</div>
        <ButtonSecondarySmall 
          onClick={() => { props.onDeleteMarkerType(markerType.id) }}
          style={{minWidth: '5px'}}>
          <DeleteOutline />
        </ButtonSecondarySmall>
      </div>
    })
  }
  if (props.error) {
    markerTypesLabels = <div>{props.error}</div>;
  }

  return (
    <React.Fragment>
      <ButtonSecondarySmall
        style={{ minWidth: '5px', color: '#cccccc', opacity: props.disabled ? 0.25 : 1 }}
        aria-describedby={modalId}
        onClick={handleOpenButtonClick}
        disabled={props.disabled}>
        <ListAlt />
      </ButtonSecondarySmall>
      <Popover
        id={modalId}
        open={openModal}
        anchorEl={anchorEl}
        onClose={handleMarkersModalClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}>
        <Card>
          <div className="markersModal" style={{ padding: '10px 30px' }}>
            <span style={{ display: 'flex' }}>
              <h1 className="header-text-bold font-17px" style={{ flexGrow: 1 }}>{texts.addMarker}</h1>
              {visibleSearchArea ?
                (<ButtonSecondarySmall
                  outline="true"
                  onClick={handleMarkersModalClose}
                  style={{ textAlign: 'right' }}>
                  <Close />
                </ButtonSecondarySmall>)
                : (<ButtonSecondarySmall
                  outline="true"
                  onClick={() => setVisibleSearchArea(!visibleSearchArea)}
                  style={{ textAlign: 'right' }}>
                  <Search />
                </ButtonSecondarySmall>)}
            </span>
            <br />
            <Grid container spacing={2}>
              <Grid item>
                <form>
                  <StyledInput
                    label={texts.markerName}
                    type="text"
                    value={markerText}
                    onChange={markerTextChangeHandler} />
                </form>
                <br />
                <CirclePicker
                  color={markerColor}
                  onChange={(color) => setMarkerColor(color.hex)}
                  circleSize={40}
                  width="300px"
                  colors={["#f44336", "#e91e63", "#9c27b0", "#673ab7", "#3f51b5", "#2196f3", "#03a9f4", "#00bcd4", "#009688", "#4caf50", "#8bc34a", "#cddc39", "#ffeb3b", "#ffc107", "#ff9800"]}
                />
                <br /><br />
                <Grid container>
                  <Grid item xs={6}>
                    <ButtonSecondarySmall
                      outline="true"
                      onClick={handleMarkersModalClose}
                      style={{ paddingTop: '10px' }}>
                      {texts.cancel}
                    </ButtonSecondarySmall>
                  </Grid>
                  <Grid item xs={6}>
                    <ButtonSecondary onClick={createMarkerType}>
                      {texts.save}
                    </ButtonSecondary>
                  </Grid>
                </Grid>
              </Grid>
              {visibleSearchArea && (
                <Grid item>
                  <StyledInput label={texts.searchMarker} type="text" value="" onChange={() => { }} />
                  <div style={{ height: '220px', width: '300px', overflow: 'auto' }}>
                    {markerTypesLabels}
                  </div>
                </Grid>
              )}
            </Grid>
          </div>
        </Card>
      </Popover>
    </React.Fragment>
  );
}

MarkersModalButton.propTypes = {
  markerTypes: PropTypes.array,
  error: PropTypes.string,
  onGetMarkerTypes: PropTypes.func,
  onCreateMarkerType: PropTypes.func,
  onAddMarkerType: PropTypes.func,
  onSetMarkerTypeOnLayer: PropTypes.func,
  onDeleteMarkerType: PropTypes.func,
  layerId: PropTypes.number,
  disabled: PropTypes.bool
};

const mapStateToProps = state => {
  return {
    markerTypes: state.videoAnalysis.markerTypes,
    error: state.videoAnalysis.markerTypeError
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onCreateMarkerType: (name, color, layerId) => dispatch(actions.createMarkerType(name, color, layerId)),
    onSetMarkerTypeOnLayer: (layerId, markerTypeId, color, name) => dispatch(actions.setMarkerTypeOnLayer(layerId, markerTypeId, color, name)),
    onDeleteMarkerType: (id) => dispatch(actions.deleteMarkerType(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(MarkersModalButton));