import { Tools } from 'react-sketch';

export const VideoToolTypes = {
  pencil: {type: Tools.Pencil, lineWidth: 3},
  brush: {type: Tools.Pencil, lineWidth: 15},
  circle: {type: Tools.Circle, lineWidth: 8},
  rectangle: {type: Tools.Rectangle, lineWidth: 8},
  eraser: 'eraser'
}