export const UserRoles = {
  admin: 'app_admin',
  founder: 'founder',
  coach: 'coach',
  player: 'player',
}