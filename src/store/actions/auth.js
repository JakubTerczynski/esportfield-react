import * as actionTypes from './actionTypes';

export const loginStart = () => {
  return {
    type: actionTypes.LOGIN_START,
  }
}

export const loginSuccess = (token, email, userId) => {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    token: token,
    email: email,
    userId: userId
  }
}

export const loginFail = (error) => {
  return {
    type: actionTypes.LOGIN_FAIL,
    error: error
  }
}

export const login = (email, password) => {
  return {
    type: actionTypes.LOGIN,
    email: email,
    password: password
  }
}

export const logout = () => {
  return {
    type: actionTypes.LOGOUT_START
  }
}

export const logoutSuccess = () => {
  return {
    type: actionTypes.LOGOUT_SUCCESS,
  }
}

export const autoLogin = () => {
  return {
    type: actionTypes.AUTO_LOGIN,
  }
}