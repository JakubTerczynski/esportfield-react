import * as actionTypes from './actionTypes';

export const createVideoLayer = (gameAnalysisId, frameFrom, frameTo) => {
  return {
    type: actionTypes.CREATE_VIDEO_LAYER,
    gameAnalysisId: gameAnalysisId,
    frameFrom: frameFrom,
    frameTo: frameTo
  }
}

export const createVideoLayerSuccess = (videoLayer) => {
  return {
    type: actionTypes.CREATE_VIDEO_LAYER_SUCCESS,
    videoLayer: videoLayer
  }
}

export const createVideoLayerFail = (error) => {
  return {
    type: actionTypes.CREATE_VIDEO_LAYER_FAIL,
    error: error
  }
}

export const deleteVideoLayer = (id) => {
  return {
    type: actionTypes.DELETE_VIDEO_LAYER,
    id: id
  }
}

export const deleteVideoLayerSuccess = (id) => {
  return {
    type: actionTypes.DELETE_VIDEO_LAYER_SUCCESS,
    id: id
  }
}

export const setVideoLayers = (videoLayers) => {
  return {
    type: actionTypes.SET_VIDEO_LAYERS,
    videoLayers: videoLayers
  }
}

export const getVideoLayers = (gameAnalysisId) => {
  return {
    type: actionTypes.GET_VIDEO_LAYERS,
    gameAnalysisId: gameAnalysisId
  }
}

export const getVideoLayersFail = (error) => {
  return {
    type: actionTypes.GET_VIDEO_LAYERS_FAIL,
    error: error
  }
}

export const setDrawLayerProps = (id, toolType) => {
  return {
    type: actionTypes.SET_DRAW_LAYER_PROPS,
    id: id,
    toolType: toolType
  }
}

export const getMarkerTypes = () => {
  return {
    type: actionTypes.GET_MARKER_TYPES
  }
}

export const getMarkerTypesSuccess = (markerTypes) => {
  return {
    type: actionTypes.GET_MARKER_TYPES_SUCCESS,
    markerTypes: markerTypes
  }
}

export const getMarkerTypesFail = (error) => {
  return {
    type: actionTypes.GET_MARKER_TYPES_FAIL,
    error: error
  }
}

export const createMarkerType = (name, color, layerId) => {
  return {
    type: actionTypes.CREATE_MARKER_TYPE,
    name: name,
    color: color,
    layerId: layerId
  }
}

export const createMarkerTypeSuccess = (markerType) => {
  return {
    type: actionTypes.CREATE_MARKER_TYPE_SUCCESS,
    markerType: markerType
  }
}

export const deleteMarkerType = (id) => {
  return {
    type: actionTypes.DELETE_MARKER_TYPE,
    id: id
  }
}

export const deleteMarkerTypeSuccess = (id) => {
  return {
    type: actionTypes.DELETE_MARKER_TYPE_SUCCESS,
    id: id
  }
}

export const setMarkerTypeOnLayer = (layerId, markerTypeId, color, name) => {
  return {
    type: actionTypes.SET_MARKER_TYPE_ON_LAYER,
    layerId: layerId,
    markerTypeId: markerTypeId,
    color: color,
    name: name
  }
}

export const setMarkerTypeOnLayerSuccess = (layerId, markerType) => {
  return {
    type: actionTypes.SET_MARKER_TYPE_ON_LAYER_SUCCESS,
    layerId: layerId,
    markerType: markerType
  }
}

export const setMarkerTypeOnLayerFail = (error) => {
  return {
    type: actionTypes.SET_MARKER_TYPE_ON_LAYER_FAIL,
    error: error
  }
}

export const setImageOnLayer = (layerId, image) => {
  return {
    type: actionTypes.SET_IMAGE_ON_LAYER,
    layerId: layerId,
    image: image
  }
}

export const setImageOnLayerSuccess = (layerId, image) => {
  return {
    type: actionTypes.SET_IMAGE_ON_LAYER_SUCCESS,
    layerId: layerId,
    image: image
  }
}

export const setImageOnLayerFail = (error) => {
  return {
    type: actionTypes.SET_IMAGE_ON_LAYER_FAIL,
    error: error    
  }
}

export const setCommentOnLayer = (layerId, comment) => {
  return {
    type: actionTypes.SET_COMMENT_ON_LAYER,
    layerId: layerId,
    comment: comment
  }
}

export const setCommentOnLayerSuccess = (layerId, comment) => {
  return {
    type: actionTypes.SET_COMMENT_ON_LAYER_SUCCESS,
    layerId: layerId,
    comment: comment
  }
}

export const setCommentOnLayerFail = (error) => {
  return {
    type: actionTypes.SET_COMMENT_ON_LAYER_FAIL,
    error: error
  }
}

export const setShowCommentLayerId = (id) => {
  return {
    type: actionTypes.SET_SHOW_COMMENT_LAYER_ID,
    id: id
  }
}

export const setVisibleLayer = (indexLayer, visible) => {
  return {
    type: actionTypes.SET_VISIBLE_LAYER,
    indexLayer: indexLayer,
    visible: visible
  }
}

export const playOnlyOneVideoLayer = (indexLayer) => {
  return {
    type: actionTypes.PLAY_ONLY_ONE_VIDEO_LAYER,
    indexLayer: indexLayer
  }
}

export const getVideoAnalysis = (id) => {
  return {
    type: actionTypes.GET_VIDEO_ANALYSIS,
    id: id
  }
}

export const getVideoAnalysisSuccess = (videoAnalysis) => {
  return {
    type: actionTypes.GET_VIDEO_ANALYSIS_SUCCESS,
    videoAnalysis: videoAnalysis
  }
}

export const getVideoAnalysisFail = (error) => {
  return {
    type: actionTypes.GET_VIDEO_ANALYSIS_FAIL,
    error: error
  }
}

export const createVideoAnalysis = (videoId, summary, recommendation, coachId) => {
  return {
    type: actionTypes.CREATE_VIDEO_ANALYSIS,
    videoId: videoId, 
    summary: summary, 
    recommendation: recommendation, 
    coachId: coachId
  }
}

export const createVideoAnalysisSuccess = (videoAnalysis) => {
  return {
    type: actionTypes.CREATE_VIDEO_ANALYSIS_SUCCESS,
    videoAnalysis: videoAnalysis    
  }
}

export const createVideoAnalysisFail = (error) => {
  return {
    type: actionTypes.CREATE_VIDEO_ANALYSIS_FAIL,
    error: error
  }
}

export const updateVideoAnalysis = (id, summary, recommendation) => {
  return {
    type: actionTypes.UPDATE_VIDEO_ANALYSIS,
    id: id,
    summary: summary,
    recommendation: recommendation
  }
}

export const updateVideoAnalysisSuccess = (summary, recommendation) => {
  return {
    type: actionTypes.UPDATE_VIDEO_ANALYSIS_SUCCESS,
    summary: summary,
    recommendation: recommendation
  }
}

export const updateVideoAnalysisFail = (error) => {
  return {
    type: actionTypes.UPDATE_VIDEO_ANALYSIS_FAIL,
    error: error
  }
}

export const getVideoAnalysisByCoachId = (coachId) => {
  return {
    type: actionTypes.GET_VIDEO_ANALYSIS_BY_COACH_ID,
    coachId: coachId
  }
}

export const getVideoAnalysisByCoachIdSuccess = (videoAnalysis) => {
  return {
    type: actionTypes.GET_VIDEO_ANALYSIS_BY_COACH_ID_SUCCESS,
    videoAnalysis: videoAnalysis
  }
}

export const getVideoAnalysisByCoachIdFail = (error) => {
  return {
    type: actionTypes.GET_VIDEO_ANALYSIS_BY_COACH_ID_FAIL,
    error: error
  }
}