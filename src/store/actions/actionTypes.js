export const LOGIN = 'LOGIN';
export const LOGIN_START = 'LOGIN_START';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';
export const LOGOUT_START = 'LOGOUT_START';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const AUTO_LOGIN = 'AUTO_LOGIN';

export const AUTO_SET_LANGUAGE_INIT = 'AUTO_SET_LANGUAGE_INIT';
export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';
export const CHANGE_LANGUAGE_SUCCESS = 'CHANGE_LANGUAGE_SUCCESS';

export const OPEN_REGISTER_MODAL = 'OPEN_REGISTER_MODAL';
export const CLOSE_REGISTER_MODAL = 'CLOSE_REGISTER_MODAL';
export const REGISTER = 'REGISTER';
export const REGISTER_START = 'REGISTER_START';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAIL = 'REGISTER_FAIL';

export const GET_GAMES = 'GET_GAMES';
export const GET_GAMES_START = 'GET_GAMES_START';
export const GET_GAMES_SUCCESS = 'GET_GAMES_SUCCESS';
export const GET_GAMES_FAIL = 'GET_GAMES_FAIL';

export const GET_USER_ROLES = 'GET_USER_ROLES';
export const GET_USER_ROLES_SUCCESS = 'GET_USER_ROLES_SUCCESS';
export const GET_USER_ROLES_FAIL = 'GET_USER_ROLES_FAIL';

export const CREATE_VIDEO_LAYER = 'CREATE_VIDEO_LAYER';
export const CREATE_VIDEO_LAYER_SUCCESS = 'CREATE_VIDEO_LAYER_SUCCESS';
export const CREATE_VIDEO_LAYER_FAIL = 'CREATE_VIDEO_LAYER_FAIL';
export const DELETE_VIDEO_LAYER = 'DELETE_VIDEO_LAYER';
export const DELETE_VIDEO_LAYER_SUCCESS = 'DELETE_VIDEO_LAYER_SUCCESS';
export const GET_VIDEO_LAYERS = 'GET_VIDEO_LAYERS';
export const SET_VIDEO_LAYERS = 'SET_VIDEO_LAYERS';
export const GET_VIDEO_LAYERS_FAIL = 'GET_VIDEO_LAYERS_FAIL';
export const SET_DRAW_LAYER_PROPS = 'SET_DRAW_LAYER_PROPS';
export const GET_MARKER_TYPES = 'GET_MARKER_TYPES';
export const GET_MARKER_TYPES_SUCCESS = 'GET_MARKER_TYPES_SUCCESS';
export const GET_MARKER_TYPES_FAIL = 'GET_MARKER_TYPES_FAIL';
export const CREATE_MARKER_TYPE = 'CREATE_MARKER_TYPE';
export const CREATE_MARKER_TYPE_SUCCESS = 'CREATE_MARKER_TYPE_SUCCESS';
export const DELETE_MARKER_TYPE = 'DELETE_MARKER_TYPE';
export const DELETE_MARKER_TYPE_SUCCESS = 'DELETE_MARKER_TYPE_SUCCESS';
export const SET_MARKER_TYPE_ON_LAYER = 'SET_MARKER_TYPE_ON_LAYER';
export const SET_MARKER_TYPE_ON_LAYER_SUCCESS = 'SET_MARKER_TYPE_ON_LAYER_SUCCESS';
export const SET_MARKER_TYPE_ON_LAYER_FAIL = 'SET_MARKER_TYPE_ON_LAYER_FAIL';
export const SET_IMAGE_ON_LAYER = 'SET_IMAGE_ON_LAYER';
export const SET_IMAGE_ON_LAYER_SUCCESS = 'SET_IMAGE_ON_LAYER_SUCCESS';
export const SET_IMAGE_ON_LAYER_FAIL = 'SET_IMAGE_ON_LAYER_FAIL';
export const SET_COMMENT_ON_LAYER = 'SET_COMMENT_ON_LAYER';
export const SET_COMMENT_ON_LAYER_SUCCESS = 'SET_COMMENT_ON_LAYER_SUCCESS';
export const SET_COMMENT_ON_LAYER_FAIL = 'SET_COMMENT_ON_LAYER_FAIL';
export const SET_SHOW_COMMENT_LAYER_ID = 'SET_SHOW_COMMENT_LAYER_ID';
export const SET_VISIBLE_LAYER = 'SET_VISIBLE_LAYER';
export const PLAY_ONLY_ONE_VIDEO_LAYER = 'PLAY_ONLY_ONE_VIDEO_LAYER';
export const GET_VIDEO_ANALYSIS = 'GET_VIDEO_ANALYSIS';
export const GET_VIDEO_ANALYSIS_SUCCESS = 'GET_VIDEO_ANALYSIS_SUCCESS';
export const GET_VIDEO_ANALYSIS_FAIL = 'GET_VIDEO_ANALYSIS_FAIL';
export const CREATE_VIDEO_ANALYSIS = 'CREATE_VIDEO_ANALYSIS';
export const CREATE_VIDEO_ANALYSIS_SUCCESS = 'CREATE_VIDEO_ANALYSIS_SUCCESS';
export const CREATE_VIDEO_ANALYSIS_FAIL = 'CREATE_VIDEO_ANALYSIS_FAIL';
export const UPDATE_VIDEO_ANALYSIS = 'UPDATE_VIDEO_ANALYSIS';
export const UPDATE_VIDEO_ANALYSIS_SUCCESS = 'UPDATE_VIDEO_ANALYSIS_SUCCESS';
export const UPDATE_VIDEO_ANALYSIS_FAIL = 'UPDATE_VIDEO_ANALYSIS_FAIL';
export const GET_VIDEO_ANALYSIS_BY_COACH_ID = 'GET_VIDEO_ANALYSIS_BY_COACH_ID';
export const GET_VIDEO_ANALYSIS_BY_COACH_ID_SUCCESS = 'GET_VIDEO_ANALYSIS_BY_COACH_ID_SUCCESS';
export const GET_VIDEO_ANALYSIS_BY_COACH_ID_FAIL = 'GET_VIDEO_ANALYSIS_BY_COACH_ID_FAIL';
