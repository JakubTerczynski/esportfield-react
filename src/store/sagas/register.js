import {put} from 'redux-saga/effects';
import * as actions from '../actions/index';
import axios from '../../axios-instance';

export function* registerSaga(action) {
  yield put(actions.registerStart());
  try {
    const response = yield axios.post('users/custom', {user: action.userData});
    yield localStorage.setItem('token', response.data.authentication_token);
    yield localStorage.setItem('email', response.data.email);
    yield localStorage.setItem('user_id', response.data.id);
    yield put(actions.loginSuccess(response.data.authentication_token, response.data.email, response.data.id));
    yield put(actions.registerSuccess());
  } catch (error) {
    yield put(actions.registerFail('Wystąpił błąd!'));
  }
}