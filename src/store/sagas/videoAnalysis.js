import { put } from 'redux-saga/effects';
import * as actions from '../actions/index';
import axios from '../../axios-instance';
import {dataURLtoFile} from '../../shared/utility';

export function* getMarkerTypesSaga(action) {
  try {
    const response = yield axios.get('marker_types');
    yield put(actions.getMarkerTypesSuccess(response.data));
  } catch (error) {
    yield put(actions.getMarkerTypesFail('Wystąpił błąd!'));
  }
}

export function* createMarkerTypeSaga(action) {
  try {
    const response = yield axios.post('marker_types', { marker_type: { name: action.name, color: action.color } });
    yield put(actions.createMarkerTypeSuccess(response.data));
    yield put(actions.setMarkerTypeOnLayerSuccess(action.layerId, response.data));
  } catch (error) {
    console.log(error);
  }
}

export function* getVideoLayersSaga(action) {
  try {
    const response = yield axios.get(`game_analysis_markers/index_by_game_analysis/${action.gameAnalysisId}`);
    yield put(actions.setVideoLayers(response.data));
  } catch (error) {
    console.log(error);
    // yield put(actions.getVideoLayers('Wystąpił błąd!'));
  }
}

export function* deleteVideoLayerSaga(action) {
  try {
    yield axios.delete(`game_analysis_markers/${action.id}`);
    yield put(actions.deleteVideoLayerSuccess(action.id));
  } catch (error) {
    console.log(error);
  }
}

export function* createVideoLayerSaga(action) {
  try {
    const response = yield axios.post('game_analysis_markers', { game_analysis_marker: { 
      game_analysis_id: action.gameAnalysisId,
      frame_from: action.frameFrom,
      frame_to: action.frameTo
     } });
    yield put(actions.createVideoLayerSuccess(response.data));
  } catch (error) {
    yield put(actions.createVideoLayerFail(error));
  }
}

export function* setMarkerTypeOnLayerSaga(action) {
  try {
    const response = yield axios.patch(`game_analysis_markers/${action.layerId}`, { game_analysis_marker: { 
      marker_type_id: action.markerTypeId
    } });
    yield put(actions.setMarkerTypeOnLayerSuccess(action.layerId, response.data.marker_type));
  } catch (error) {
    yield put(actions.setMarkerTypeOnLayerFail(error));
  }
}

export function* setCommentOnLayerSaga(action) {
  try {
    const response = yield axios.patch(`game_analysis_markers/${action.layerId}`, { game_analysis_marker: { comment: action.comment } });
    yield put(actions.setCommentOnLayerSuccess(action.layerId, response.data.comment));
  } catch (error) {
    yield put(actions.setCommentOnLayerFail(error));
  }
}

export function* setImageOnLayerSaga(action) {
  try {
    const file = dataURLtoFile(action.image, `${new Date()}_layer${action.layerId}.png`);
    const formData = new FormData();
    formData.append('game_analysis_marker[image]', file);
    const response = yield axios.patch(`game_analysis_markers/${action.layerId}`, formData, { headers: {'content-type': 'multipart/form-data'} });
    yield put(actions.setImageOnLayerSuccess(action.layerId, response.data.image));
  } catch (error) {
    yield put(actions.setImageOnLayerFail(error));
  }
}

export function* deleteMarkerTypeSaga(action) {
  try {
    yield axios.delete(`marker_types/${action.id}`);
    yield put(actions.deleteMarkerTypeSuccess(action.id));
  } catch (error) {
    console.log(error);
  }
}

export function* getVideoAnalysisSaga(action) {
  try {
    const response = yield axios.get(`game_analyses/${action.id}`);
    yield put(actions.getVideoAnalysisSuccess(response.data));
  } catch (error) {
    yield put(actions.getVideoAnalysisFail('Wystąpił błąd!'));
  }
}

export function* createVideoAnalysisSaga(action) {
  try {
    const response = yield axios.post(`game_analyses`, { game_analysis: {
      game_video_id: action.videoId, 
      summary: action.summary, 
      recommendation: action.recommendation, 
      coach_id: action.coachId
    } });
    yield put(actions.createVideoAnalysisSuccess(response.data));
  } catch (error) {
    yield put(actions.createVideoAnalysisFail('Wystąpił błąd!'));
  }
}

export function* updateVideoAnalysisSaga(action) {
  try {
    const response = yield axios.patch(`game_analyses/${action.id}`, { game_analysis: {
      summary: action.summary, 
      recommendation: action.recommendation
    } });
    yield put(actions.updateVideoAnalysisSuccess(response.data.summary, response.data.recommendation));
  } catch (error) {
    yield put(actions.updateVideoAnalysisFail('Wystąpił błąd!'));
  }
}

export function* getVideoAnalysisByCoachIdSaga(action) {
  try {
    const response = yield axios.get(`game_analyses/index_by_coach/${action.coachId}`);
    yield put(actions.getVideoAnalysisByCoachIdSuccess(response.data));
  } catch (error) {
    yield put(actions.getVideoAnalysisByCoachIdFail('Wystąpił błąd!'));
  }
}