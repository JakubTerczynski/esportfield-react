import {put} from 'redux-saga/effects';
import * as actions from '../actions/index';
import axios from '../../axios-instance';

export function* loginSaga(action) {
  yield put(actions.loginStart());
  const loginData = {
    email: action.email,
    password: action.password
  }
  try {
    const response = yield axios.post('users/sessions', loginData);
    yield localStorage.setItem('token', response.data.authentication_token);
    yield localStorage.setItem('email', response.data.email);
    yield localStorage.setItem('user_id', response.data.id);
    yield put(actions.loginSuccess(response.data.authentication_token, response.data.email, response.data.id));
  } catch (error) {
    yield put(actions.loginFail('Niepoprawne logowanie!'));
  }
}

export function* logoutSaga(action) {
  const token = yield localStorage.getItem('token');
  const email = yield localStorage.getItem('email');
  const userId = yield localStorage.getItem('user_id');
  try {
    yield axios.delete('users/sessions', {
      headers: {
        'X-User-Email': email,
        'X-User-Token': token
      }
    });
    yield put(actions.logoutSuccess());
  } catch (error) {
    yield put(actions.loginSuccess(token, email, userId));
  }
}

export function* autoLoginSaga(action) {
  const token = yield localStorage.getItem('token');
  if (!token) {
    yield put(actions.logoutSuccess());
  } else {
    const email = yield localStorage.getItem('email');
    const userId = yield localStorage.getItem('user_id');
    yield put(actions.loginSuccess(token, email, userId));
  }
}

export function* logoutSuccessSaga(action) {
  yield localStorage.removeItem('token');
  yield localStorage.removeItem('email');
  yield localStorage.removeItem('user_id');
}