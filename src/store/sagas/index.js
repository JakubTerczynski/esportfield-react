import {takeEvery} from 'redux-saga/effects';
import * as actionTypes from '../actions/actionTypes';
import {loginSaga, logoutSaga, logoutSuccessSaga, autoLoginSaga} from './auth';
import {autoSetLanguageSaga, changeLanguageSaga} from './language';
import {registerSaga} from './register';
import {getGamesSaga} from './games';
import {getUserRolesSaga} from './userRoles';
import {getMarkerTypesSaga, 
  createMarkerTypeSaga, 
  getVideoLayersSaga, 
  deleteVideoLayerSaga,
  createVideoLayerSaga,
  setMarkerTypeOnLayerSaga,
  setImageOnLayerSaga,
  setCommentOnLayerSaga,
  deleteMarkerTypeSaga,
  getVideoAnalysisSaga,
  createVideoAnalysisSaga,
  updateVideoAnalysisSaga,
  getVideoAnalysisByCoachIdSaga} from './videoAnalysis';

export function* watchAuth() {
  yield takeEvery(actionTypes.LOGIN, loginSaga);
  yield takeEvery(actionTypes.LOGOUT_START, logoutSaga);
  yield takeEvery(actionTypes.LOGOUT_SUCCESS, logoutSuccessSaga);
  yield takeEvery(actionTypes.AUTO_LOGIN, autoLoginSaga);
}

export function* watchLanguage() {
  yield takeEvery(actionTypes.AUTO_SET_LANGUAGE_INIT, autoSetLanguageSaga);
  yield takeEvery(actionTypes.CHANGE_LANGUAGE, changeLanguageSaga);
}

export function* watchRegister() {
  yield takeEvery(actionTypes.REGISTER, registerSaga);
}

export function* watchGames() {
  yield takeEvery(actionTypes.GET_GAMES, getGamesSaga);
}

export function* watchUserRoles() {
  yield takeEvery(actionTypes.GET_USER_ROLES, getUserRolesSaga);
}

export function* watchVideoAnalysis() {
  yield takeEvery(actionTypes.GET_MARKER_TYPES, getMarkerTypesSaga);
  yield takeEvery(actionTypes.CREATE_MARKER_TYPE, createMarkerTypeSaga);
  yield takeEvery(actionTypes.GET_VIDEO_LAYERS, getVideoLayersSaga);
  yield takeEvery(actionTypes.DELETE_VIDEO_LAYER, deleteVideoLayerSaga);
  yield takeEvery(actionTypes.CREATE_VIDEO_LAYER, createVideoLayerSaga);
  yield takeEvery(actionTypes.SET_MARKER_TYPE_ON_LAYER, setMarkerTypeOnLayerSaga);
  yield takeEvery(actionTypes.SET_IMAGE_ON_LAYER, setImageOnLayerSaga);
  yield takeEvery(actionTypes.SET_COMMENT_ON_LAYER, setCommentOnLayerSaga);
  yield takeEvery(actionTypes.DELETE_MARKER_TYPE, deleteMarkerTypeSaga);
  yield takeEvery(actionTypes.GET_VIDEO_ANALYSIS, getVideoAnalysisSaga);
  yield takeEvery(actionTypes.CREATE_VIDEO_ANALYSIS, createVideoAnalysisSaga);
  yield takeEvery(actionTypes.UPDATE_VIDEO_ANALYSIS, updateVideoAnalysisSaga);
  yield takeEvery(actionTypes.GET_VIDEO_ANALYSIS_BY_COACH_ID, getVideoAnalysisByCoachIdSaga);
}