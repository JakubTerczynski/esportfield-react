import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
  videoAnalysis: null,
  videoAnalysisByCoachId: null,
  videoLayers: [],
  error: null,
  markerTypes: [],
  markerTypesError: null,
  drawLayerId: null,
  drawToolType: null,
  showCommentLayerId: null,
  playVideoLayer: null
}

const createVideoLayerSuccess = (state, action) => {
  const videoLayer = updateObject(action.videoLayer, { visible: true });
  const updatedVideoLayers = state.videoLayers.concat(videoLayer);
  return updateObject(state, { videoLayers: updatedVideoLayers, drawLayerId: null, drawToolType: null, showCommentLayerId: null });
}

const createVideoLayerFail = (state, action) => {
  return updateObject(state, { error: action.error });
}

const deleteVideoLayerSuccess = (state, action) => {
  const updatedVideoLayers = state.videoLayers.filter(layer => layer.id !== action.id);
  return updateObject(state, { videoLayers: updatedVideoLayers, drawLayerId: null, drawToolType: null, showCommentLayerId: null });
}

const setVideoLayers = (state, action) => {
  const updatedVideoLayers = action.videoLayers.map(layer => updateObject(layer, { visible: true }));
  return updateObject(state, { videoLayers:  updatedVideoLayers});
}

const getVideoLayersFail = (state, action) => {
  return updateObject(state, { error: action.error });
}

const setDrawLayerProps = (state, action) => {
  return updateObject(state, { drawLayerId: action.id, drawToolType: action.toolType });
}

const getMarkerTypesFail = (state, action) => {
  return updateObject(state, { markerTypesError: action.error });
}

const getMarkerTypesSuccess = (state, action) => {
  return updateObject(state, { markerTypes: action.markerTypes });
}

const createMarkerTypeSuccess = (state, action) => {
  const updatedMarkerTypes = state.markerTypes.concat(action.markerType);
  return updateObject(state, { markerTypes: updatedMarkerTypes });
}

const deleteMarkerTypeSuccess = (state, action) => {
  const updatedMarkerTypes = state.markerTypes.filter(markerType => markerType.id !== action.id);
  return updateObject(state, { markerTypes: updatedMarkerTypes });
}

const setMarkerTypeOnLayerSuccess = (state, action) => {
  const updatedVideoLayers = state.videoLayers.map(layer =>
    layer.id === action.layerId ? updateObject(layer, { marker_type: action.markerType }) : layer);
  return updateObject(state, { videoLayers: updatedVideoLayers });
}

const setMarkerTypeOnLayerFail = (state, action) => {
  return updateObject(state, { error: action.error });
}

const setImageOnLayerSuccess = (state, action) => {
  const updatedVideoLayers = state.videoLayers.map(layer =>
    layer.id === action.layerId ? updateObject(layer, { image: action.image }) : layer);
  return updateObject(state, { videoLayers: updatedVideoLayers });
}

const setImageOnLayerFail = (state, action) => {
  return updateObject(state, { error: action.error });
}

const setCommentOnLayerSuccess = (state, action) => {
  const updatedVideoLayers = state.videoLayers.map(layer =>
    layer.id === action.layerId ? updateObject(layer, { comment: action.comment }) : layer);
  return updateObject(state, { videoLayers: updatedVideoLayers });
}

const setCommentOnLayerFail = (state, action) => {
  return updateObject(state, { error: action.error });
}

const setShowCommentLayerId = (state, action) => {
  return updateObject(state, { showCommentLayerId: action.id });
}

const setVisibleLayer = (state, action) => {
  const updatedVideoLayers = state.videoLayers.map((layer, index) =>
    index === action.indexLayer ? updateObject(layer, { visible: action.visible }) : layer);
  return updateObject(state, { videoLayers: updatedVideoLayers });
}

const playOnlyOneVideoLayer = (state, action) => {
  let updatedVideoLayers = state.videoLayers.map(layer => updateObject(layer, { visible: true }));
  let playVideoLayerObject = null;
  if (action.indexLayer !== null) {
    updatedVideoLayers = state.videoLayers.map((layer, index) =>
      index === action.indexLayer ? updateObject(layer, { visible: true }) : updateObject(layer, { visible: false }));
    const videoLayer = state.videoLayers.find((layer, index) => index === action.indexLayer);
    playVideoLayerObject = {
      index: action.indexLayer,
      frame_from: videoLayer.frame_from,
      frame_to: videoLayer.frame_to
    };
  }
  return updateObject(state, { videoLayers: updatedVideoLayers, playVideoLayer: playVideoLayerObject });
}

const getVideoAnalysisFail = (state, action) => {
  return updateObject(state, { error: action.error });
}

const getVideoAnalysisSuccess = (state, action) => {
  return updateObject(state, { videoAnalysis: action.videoAnalysis });
}

const createVideoAnalysisFail = (state, action) => {
  return updateObject(state, { error: action.error });
}

const createVideoAnalysisSuccess = (state, action) => {
  return updateObject(state, { videoAnalysis: action.videoAnalysis });
}

const updateVideoAnalysisFail = (state, action) => {
  return updateObject(state, { error: action.error });
}

const updateVideoAnalysisSuccess = (state, action) => {
  const updateVideoAnalysis = updateObject(state.videoAnalysis, { 
    summary: action.summary, 
    recommendation: action.recommendation
  });
  return updateObject(state, { videoAnalysis: updateVideoAnalysis });
}

const getVideoAnalysisByCoachIdFail = (state, action) => {
  return updateObject(state, { error: action.error });
}

const getVideoAnalysisByCoachIdSuccess = (state, action) => {
  return updateObject(state, { videoAnalysisByCoachId: action.videoAnalysis });
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case (actionTypes.CREATE_VIDEO_LAYER_SUCCESS): return createVideoLayerSuccess(state, action);
    case (actionTypes.CREATE_VIDEO_LAYER_FAIL): return createVideoLayerFail(state, action);
    case (actionTypes.DELETE_VIDEO_LAYER_SUCCESS): return deleteVideoLayerSuccess(state, action);
    case (actionTypes.SET_VIDEO_LAYERS): return setVideoLayers(state, action);
    case (actionTypes.GET_VIDEO_LAYERS_FAIL): return getVideoLayersFail(state, action);
    case (actionTypes.SET_DRAW_LAYER_PROPS): return setDrawLayerProps(state, action);
    case (actionTypes.GET_MARKER_TYPES_FAIL): return getMarkerTypesFail(state, action);
    case (actionTypes.GET_MARKER_TYPES_SUCCESS): return getMarkerTypesSuccess(state, action);
    case (actionTypes.CREATE_MARKER_TYPE_SUCCESS): return createMarkerTypeSuccess(state, action);
    case (actionTypes.DELETE_MARKER_TYPE_SUCCESS): return deleteMarkerTypeSuccess(state, action);
    case (actionTypes.SET_MARKER_TYPE_ON_LAYER_SUCCESS): return setMarkerTypeOnLayerSuccess(state, action);
    case (actionTypes.SET_MARKER_TYPE_ON_LAYER_FAIL): return setMarkerTypeOnLayerFail(state, action);
    case (actionTypes.SET_IMAGE_ON_LAYER_SUCCESS): return setImageOnLayerSuccess(state, action);
    case (actionTypes.SET_IMAGE_ON_LAYER_FAIL): return setImageOnLayerFail(state, action);
    case (actionTypes.SET_COMMENT_ON_LAYER_SUCCESS): return setCommentOnLayerSuccess(state, action);
    case (actionTypes.SET_COMMENT_ON_LAYER_FAIL): return setCommentOnLayerFail(state, action);
    case (actionTypes.SET_SHOW_COMMENT_LAYER_ID): return setShowCommentLayerId(state, action);
    case (actionTypes.SET_VISIBLE_LAYER): return setVisibleLayer(state, action);
    case (actionTypes.PLAY_ONLY_ONE_VIDEO_LAYER): return playOnlyOneVideoLayer(state, action);
    case (actionTypes.GET_VIDEO_ANALYSIS_SUCCESS): return getVideoAnalysisSuccess(state, action);
    case (actionTypes.GET_VIDEO_ANALYSIS_FAIL): return getVideoAnalysisFail(state, action);
    case (actionTypes.CREATE_VIDEO_ANALYSIS_SUCCESS): return createVideoAnalysisSuccess(state, action);
    case (actionTypes.CREATE_VIDEO_ANALYSIS_FAIL): return createVideoAnalysisFail(state, action);
    case (actionTypes.UPDATE_VIDEO_ANALYSIS_SUCCESS): return updateVideoAnalysisSuccess(state, action);
    case (actionTypes.UPDATE_VIDEO_ANALYSIS_FAIL): return updateVideoAnalysisFail(state, action);
    case (actionTypes.GET_VIDEO_ANALYSIS_BY_COACH_ID_SUCCESS): return getVideoAnalysisByCoachIdSuccess(state, action);
    case (actionTypes.GET_VIDEO_ANALYSIS_BY_COACH_ID_FAIL): return getVideoAnalysisByCoachIdFail(state, action);
    default: return state;
  }
}

export default reducer;