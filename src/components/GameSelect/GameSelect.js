import React, {useEffect} from 'react';
import { FormControl, Select, MenuItem, OutlinedInput, makeStyles, InputLabel } from '@material-ui/core';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Spinner from '../UI/Spinner/Spinner';
import { injectIntl } from 'react-intl';

const useStyles = makeStyles(theme => ({
  Select: {
    fontFamily: 'Poppins-Regular',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '14px',

    '& fieldset': {
      borderRadius: '10px'
    }
  }
}));

const GameSelect = ({intl, ...props}) => {
  const uiClasses = useStyles();
  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);

  useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  const texts = {
    game: intl.formatMessage({
      id: 'game'
    })
  };

  const games = props.games.map(game =>
    <MenuItem 
      key={game.id} 
      value={game.id} 
      className={uiClasses.Select}>{game.name}</MenuItem>);

  let errorMessage = null;
  if (props.error) {
    errorMessage = (
      <p style={{ color: 'red', fontWeight: 'bold' }}>{props.error}</p>
    );
  }

  let select = (
    <Select
      value={props.value === 0 ? '' : props.value}
      onChange={props.onChange}
      input={<OutlinedInput labelWidth={labelWidth} id="games-label" />}
      className={uiClasses.Select}>
      {games}
    </Select>);
  if (props.loading) {
    select = <Spinner />;
  }

  return (
    <FormControl 
      fullWidth 
      variant={props.variant}
      style={{textAlign: 'left'}}>
      <InputLabel 
        className={uiClasses.Select} 
        ref={inputLabel} 
        htmlFor="games-label">
        {texts.game}
        </InputLabel> 
      {select}
      {errorMessage}
    </FormControl>
  );
}

GameSelect.propTypes = {
  variant: PropTypes.string,
  value: PropTypes.number,
  onChange: PropTypes.func,
  games: PropTypes.array,
  error: PropTypes.string,
  loading: PropTypes.bool
}

const mapStateToProps = state => {
  return {
    games: state.games.games,
    error: state.games.error,
    loading: state.games.loading
  }
}

export default connect(mapStateToProps)(injectIntl(GameSelect));