import React from 'react';
import PlayAndTrain from './PlayAndTrain/PlayAndTrain';
import SelectZone from './SelectZone/SelectZone';
import PopularPlayers from './PopularPlayers/PopularPlayers';
import FindCoach from './FindCoach/FindCoach';
import Marketplace from './Marketplace/Marketplace';
import TrainAndAnalyze from './TrainAndAnalyze/TrainAndAnalyze';
import PopularCoaches from './PopularCoaches/PopularCoaches';
import CoursesZone from './CoursesZone/CoursesZone';
import JoinOrganization from './JoinOrganization/JoinOrganization';
import PopularOrganizations from './PopularOrganizations/PopularOrganizations';
import JoinUs from './JoinUs/JoinUs';
import { Container, Grid } from '@material-ui/core';
import classes from './Dashboard.module.scss';

const Dashboard = (props) => {
  return (
    <React.Fragment>
      <section>
        <Container>
          <section className={classes.Section}
            style={{paddingTop: '0px'}}>
            <PlayAndTrain />
          </section>
        </Container>
      </section>
      <section className={classes.BgGradientBottom}>
        <Container>
          <section className={classes.Section}><SelectZone /></section>
        </Container>
      </section>
      <section className={classes.BgColor}>
        <Container>
          <section className={classes.Section}><PopularPlayers /></section>
        </Container>
      </section>
      <section className={classes.BgGradientTop}>
        <Container>
          <Grid container spacing={10}>
            <Grid item xs={12} sm={6}>
              <section className={classes.Section}><FindCoach /></section>
            </Grid>
            <Grid item xs={12} sm={6}>
              <section className={classes.Section}><Marketplace /></section>
            </Grid>
          </Grid>
        </Container>
      </section>
      <section>
        <Container>
          <Grid container spacing={10}>
            <Grid item xs={12} sm={6}>
              <section className={classes.Section}><TrainAndAnalyze /></section>
            </Grid>
            <Grid item xs={12} sm={6}>
              <section className={classes.Section}><PopularCoaches /></section>
            </Grid>
          </Grid>
        </Container>
      </section>
      <section className={classes.BgGradientBottom}>
        <Container>
          <section className={classes.Section}><CoursesZone /></section>
        </Container>
      </section>
      <section className={classes.BgColor}>
        <Container>
          <Grid container spacing={10}>
            <Grid item xs={12} sm={6}>
              <section className={classes.Section}><JoinOrganization /></section>
            </Grid>
            <Grid item xs={12} sm={6}>
              <section className={classes.Section}><PopularOrganizations /></section>
            </Grid>
          </Grid>
        </Container>
      </section>
      <section className={classes.BgGradientTop}>
        <Container>
          <section className={classes.Section}><JoinUs /></section>
        </Container>
      </section>
    </React.Fragment>
  );
}

export default Dashboard;