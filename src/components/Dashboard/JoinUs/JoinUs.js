import React from 'react';
import { injectIntl, FormattedMessage } from 'react-intl';
import playerImg from '../../../assets/images/player.svg';
import coachImg from '../../../assets/images/coach.svg';
import ButtonPrimary from '../../UI/Buttons/ButtonPrimary/ButtonPrimary';
import { Grid } from '@material-ui/core';
import classes from './JoinUs.module.scss';

const JoinUs = ({intl, ...props}) => {
  const texts = {
    joinGameNow: intl.formatMessage({
      id: 'game.joinNow'
    }),
    joinAsPlayer: intl.formatMessage({
      id: 'player.join'
    }),
    joinAsCoach: intl.formatMessage({
      id: 'coach.join'
    }),
    player: intl.formatMessage({
      id: 'player'
    }),
    coach: intl.formatMessage({
      id: 'coach'
    })
  };

  return (
    <div style={{'textAlign': 'center'}}>
      <h1 className='header-text-bold font-55px'>{texts.joinGameNow}</h1>
      <p className="header-text-regular font-17px">
      <FormattedMessage 
        id="game.joinAsPlayerOrAsCoach" 
        values={{ 
          player: <b style={{textTransform: 'uppercase'}}>{texts.player}</b>,
          coach: <b style={{textTransform: 'uppercase'}}>{texts.coach}<br /></b>
        }}/>
      </p>
      <br /><br />
      <Grid container spacing={3}>
        <Grid item xs={6} style={{textAlign: 'right'}}>
          <img src={playerImg} alt="Player" className={classes.Img} />
          <br /><br />
          <ButtonPrimary outline="true">{texts.joinAsPlayer}</ButtonPrimary>
        </Grid>
        <Grid item xs={6} style={{textAlign: 'left'}}>
          <img src={coachImg} alt="Coach" 
            className={classes.Img}
            style={{paddingLeft: '30px'}} />
          <br /><br />
          <ButtonPrimary outline="true">{texts.joinAsCoach}</ButtonPrimary>
        </Grid>
      </Grid>
    </div>
  );
}
 
export default injectIntl(JoinUs);