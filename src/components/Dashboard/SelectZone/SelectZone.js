import React from 'react';
import { Grid, Card, CardContent } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import playerImg from '../../../assets/images/player.svg';
import trainImg from '../../../assets/images/train.svg';
import organizationImg from '../../../assets/images/organization.svg';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import ButtonSecondary from '../../UI/Buttons/ButtonSecondary/ButtonSecondary';

const useStyles = makeStyles(theme => ({
  Card: {
    maxWidth: 370,
    maxHeight: 430,
    margin: 14,
    textAlign: 'center',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    borderRadius: 10,
    padding: 0
  },
  CardWhite: {
    background: 'white',
    position: 'relative',
        
    '&:before': {
      content: '""',
      top: 0,
      left: 0,
      borderWidth: '0 0 77px 77px',
      borderStyle: 'solid',
      borderColor: 'transparent #0B1422',
      width: 0
    }
  },
  CardWhiteContent: {
    marginTop: '-70px',
  },
  TextInCorner:  {
    transform: 'rotate(-45deg)',
    position: 'absolute',
    top: 0,
    left: 0,
    fontWeight: 500,
    fontSize: 11,
    lineHeight: '94.4%',
    color: 'white'
  },
  CardDark: {
    background: '#0B1422',
    color: '#FFFFFF'
  },
  CardImg: {
    padding: 10
  },
  CardText: {
    fontSize: 14,
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 5,
    paddingBottom: 10
  }
}));

const SelectZone = ({ intl, ...props }) => {
  const uiClasses = useStyles();

  const texts = {
    trainAndAnalyze: intl.formatMessage({
      id: 'common.trainAndAnalyze'
    }),
    playersZone: intl.formatMessage({
      id: 'players.zone'
    }),
    organizationsZone: intl.formatMessage({
      id: 'organizations.zone'
    }),
    select: intl.formatMessage({
      id: 'common.select'
    })
  };

  return (
    <Grid container>
      <Grid item xs={12} sm={4} component={Card} className={clsx(uiClasses.CardDark, uiClasses.Card)}>
        <CardContent>
          <h3 className='header-text-bold font-20px'>{texts.trainAndAnalyze}</h3>
          <img className={uiClasses.CardImg} src={playerImg} alt="Player" />
          <p className={uiClasses.CardText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod.</p>
          <ButtonSecondary outline="true">{texts.select}</ButtonSecondary>
        </CardContent>
      </Grid>
      <Grid item xs={12} sm={4} component={Card} className={clsx(uiClasses.CardWhite, uiClasses.Card)}>
        <p className={uiClasses.TextInCorner}>Most<br/>popular</p>
        <CardContent className={uiClasses.CardWhiteContent}>
          <h2 className='header-text-bold font-20px'>{texts.trainAndAnalyze}</h2>
          <img className={uiClasses.CardImg} src={trainImg} alt="Train" />
          <p className={uiClasses.CardText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod.</p>
          <ButtonSecondary>{texts.select}</ButtonSecondary>
        </CardContent>
      </Grid>
      <Grid item xs={12} sm={4} component={Card} className={clsx(uiClasses.CardDark, uiClasses.Card)}>
        <CardContent>
          <h3 className='header-text-bold font-20px'>{texts.organizationsZone}</h3>
          <img className={uiClasses.CardImg} src={organizationImg} alt="Organization" />
          <p className={uiClasses.CardText}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod.</p>
          <ButtonSecondary outline="true">{texts.select}</ButtonSecondary>
        </CardContent>
      </Grid>
    </Grid>
  );
}

export default injectIntl(SelectZone);