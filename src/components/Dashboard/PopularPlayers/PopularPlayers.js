import React from 'react';
import CardWithAvatar from '../../UI/CardWithAvatar/CardWithAvatar';
import { Grid } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import ButtonSecondarySmall from '../../UI/Buttons/ButtonSecondary/ButtonSecondarySmall/ButtonSecondarySmall';
import ButtonSecondary from '../../UI/Buttons/ButtonSecondary/ButtonSecondary';
import starInCircleImg from '../../../assets/images/star-in-circle.svg';
import earthImg from '../../../assets/images/earth.svg';
import organizationJoinImg from '../../../assets/images/organization-join.svg';

const PopularPlayers = ({intl, ...props}) => {
  const texts = {
    seeStats: intl.formatMessage({
      id: 'common.seeStats'
    }),
    seeAll: intl.formatMessage({
      id: 'common.seeAll'
    }),
    rank: intl.formatMessage({
      id: 'common.rank'
    }),
    languages: intl.formatMessage({
      id: 'common.languages'
    }),
    organization: intl.formatMessage({
      id: 'organization'
    }),
    player: intl.formatMessage({
      id: 'player'
    })
  };

  const players = popularPlayers.map(player => (
    <Grid item xs={12} sm={3} key={Math.random()}>
      <CardWithAvatar
        labelText={texts.player}
        labelColor="#0E76BC">
        <h3 className='MarginBottom5px'>
          {player.nickname} ({player.age}) {player.country}
        </h3>
        <p className='NoMarginTop'>{player.games.join(', ')}</p>
        <br />
        <p className='NoMarginTop'>
          <img src={starInCircleImg} alt="Star" />&nbsp;
          {texts.rank}: {player.rank}
        </p>
        <p>
          <img src={earthImg} alt="Earth" />&nbsp;
          {texts.languages}: {player.languages.join(', ')}
        </p>
        <p>
        <img src={organizationJoinImg} alt="Org" />&nbsp;
          {texts.organization}: {player.organization}
        </p>
        <br />
        <ButtonSecondarySmall>{texts.seeStats} >></ButtonSecondarySmall>
      </CardWithAvatar>
    </Grid>
  ));

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        {players}
      </Grid>
      <div style={{'padding': '30px', 'textAlign': 'center'}}>
        <ButtonSecondary outline="true">{texts.seeAll}</ButtonSecondary>
      </div>
    </React.Fragment>
  );
}

const defaultPlayer = {
  nickname: 'John Doe',
  age: 28,
  country: 'PL',
  games: ['CS:GO', 'LOL', 'FOTRNITE'],
  rank: 'SILVER IV',
  languages: ['PL', 'EN'],
  organization: 'Fighters'
}
const popularPlayers = [defaultPlayer, defaultPlayer, defaultPlayer, defaultPlayer];
 
export default injectIntl(PopularPlayers);