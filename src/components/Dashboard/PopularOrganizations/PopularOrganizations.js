import React from 'react';
import { Grid } from '@material-ui/core';
import CardWithAvatar from '../../UI/CardWithAvatar/CardWithAvatar';
import {StyledRating} from '../../UI/StyledRating/StyledRating';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import { injectIntl } from 'react-intl';
import ButtonSecondarySmall from '../../UI/Buttons/ButtonSecondary/ButtonSecondarySmall/ButtonSecondarySmall';
import ButtonSecondary from '../../UI/Buttons/ButtonSecondary/ButtonSecondary';
import earthImg from '../../../assets/images/earth.svg';

const PopularOrganizations = ({intl, ...props}) => {
  const texts = {
    seeFullProfile: intl.formatMessage({
      id: 'common.seeFullProfile'
    }),
    languages: intl.formatMessage({
      id: 'common.languages'
    }),
    seeAll: intl.formatMessage({
      id: 'common.seeAll'
    }),
    organization: intl.formatMessage({
      id: 'organization'
    })
  };

  const organizations = popularOrganizations.map(org => (
    <Grid item xs={12} sm={6} key={Math.random()}>
      <CardWithAvatar
        labelText={texts.organization}
        labelColor="#0F0F0F">
        <h3 className='MarginBottom5px'>{org.name}</h3>
        <StyledRating
          name="customized-empty"
          value={org.rate}
          precision={0.5}
          emptyIcon={<StarBorderIcon style={{color: '#A49620'}} fontSize="inherit" />}
          readOnly />
        <p style={{marginTop: '20px', marginBottom: '20px'}}>
          <img src={earthImg} alt="Earth" />&nbsp;
          {texts.languages}: {org.languages.join(', ')}
        </p>
        <ButtonSecondarySmall>{texts.seeFullProfile} >></ButtonSecondarySmall>
      </CardWithAvatar>
    </Grid>
  ));

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        {organizations}
      </Grid>
      <div style={{'padding': '30px', 'textAlign': 'center'}}>
        <ButtonSecondary outline="true">{texts.seeAll}</ButtonSecondary>
      </div>
    </React.Fragment>
  );
}

const defaultOrganization = {
  name: 'NAZWA ORGANIZACJI',
  rate: 4,
  languages: ['PL', 'EN']
}
const popularOrganizations = [defaultOrganization, defaultOrganization, defaultOrganization, defaultOrganization];
 
export default injectIntl(PopularOrganizations);