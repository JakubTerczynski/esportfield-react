import React from 'react';
import GamesCarousel from './GamesCarousel/GamesCarousel';
import { Grid } from '@material-ui/core';
import { injectIntl } from 'react-intl';
import ButtonPrimary from '../../UI/Buttons/ButtonPrimary/ButtonPrimary';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as actions from '../../../store/actions/index';

const PlayAndTrain = ({intl, ...props}) => {
  const texts = {
    playTrain: intl.formatMessage({
      id: 'common.playTrain'
    }),
    measureAchievements: intl.formatMessage({
      id: 'common.measureAchievements'
    }),
    startNow: intl.formatMessage({
      id: 'common.startNow'
    }),
    checkIfBetter: intl.formatMessage({
      id: 'common.checkIfBetter'
    }),
    compareStatsImproveSkills: intl.formatMessage({
      id: 'common.compareStatsImproveSkills'
    }),
    beTheBest: intl.formatMessage({
      id: 'common.beTheBest'
    })
  };

  return (
    <Grid container spacing={3}>
      <Grid item xs={12} md={6} style={{padding: '30px'}}>
        <h1 className='header-text-regular font-55px'>
          {texts.playTrain}<br />
          <b>{texts.measureAchievements}</b>
        </h1>
        <p className='header-text-regular font-17px'
          style={{lineHeight: '24px'}}>
            {texts.checkIfBetter}<br />
            {texts.compareStatsImproveSkills},<br />
            <b>{texts.beTheBest}!</b>
          </p>
        <br />
        <ButtonPrimary outline="true" onClick={props.onOpenRegisterModal}>{texts.startNow}</ButtonPrimary>
      </Grid>
      <Grid item xs={12} md={6}>
        <GamesCarousel />
      </Grid>
    </Grid>
  );
}

PlayAndTrain.propTypes = {
  onOpenRegisterModal: PropTypes.func
}

const mapDispatchToProps = dispatch => {
  return {
    onOpenRegisterModal: () => dispatch(actions.openRegisterModal()),
  }
}

export default connect(null, mapDispatchToProps)(injectIntl(PlayAndTrain));