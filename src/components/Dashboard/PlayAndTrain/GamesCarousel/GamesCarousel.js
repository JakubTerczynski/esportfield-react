import React from 'react';
import ReactCarousel from '../../../UI/ReactCarousel/ReactCarousel';
import LolImg from '../../../../assets/images/LoL.svg';
import FortniteImg from '../../../../assets/images/Fortnite.png';
import CsGoImg from '../../../../assets/images/CSgo.png';

const GamesCarousel = (props) => {
  return (
    <ReactCarousel
      width={960}
      height={420}
      displayQuantityOfSide={1}
      navigation={true}
      enableHeading={false}
      enableScroll={false}>
      <img src={FortniteImg} alt='fortnite'/>
      <img src={LolImg} alt='lol'/>
      <img src={CsGoImg} alt='co:go'/>
    </ReactCarousel>
  );
}
 
export default GamesCarousel;