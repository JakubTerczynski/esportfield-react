import React from 'react';
import { injectIntl } from 'react-intl';
import addVideoImg from '../../../assets/images/add-video.svg';
import arrowTopImg from '../../../assets/images/arrow-top.svg';
import arrowBottomImg from '../../../assets/images/arrow-bottom.svg';
import moneyImg from '../../../assets/images/money.svg';
import timeImg from '../../../assets/images/time.svg';
import ButtonPrimary from '../../UI/Buttons/ButtonPrimary/ButtonPrimary';
import classes from './FindCoach.module.scss';
import { Grid, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  TextCenter: {
    textAlign: 'center'
  }
}));

const FindCoach = ({ intl, ...props }) => {
  const uiClasses = useStyles();
  const texts = {
    findCoach: intl.formatMessage({
      id: 'coach.find'
    }),
    howItWorks: intl.formatMessage({
      id: 'common.howItWorks'
    }),
    startNow: intl.formatMessage({
      id: 'common.startNow'
    }),
    findHelpBeBetter: intl.formatMessage({
      id: 'coach.findHelpBeBetter'
    }),
    addVideo: intl.formatMessage({
      id: 'common.addVideo'
    }),
    suggestMoney: intl.formatMessage({
      id: 'common.suggestMoney'
    }),
    waitForCoachOffer: intl.formatMessage({
      id: 'common.waitForCoachOffer'
    })
  };

  return (
    <React.Fragment>
      <div>
        <h1
          className='header-text-bold font-55px'
          style={{ marginBottom: '20px' }}>
          {texts.findCoach}
        </h1>
        <p className="header-text-regular font-17px">{texts.findHelpBeBetter}!</p>
      </div>
      <br /><br />
      <div>
        <h1 className='header-text-bold font-24px'>{texts.howItWorks}?</h1><br />
        <Grid container>
          <Grid item xs={3} className={uiClasses.TextCenter}>
            <img className={classes.Img} src={addVideoImg} alt="Add video" />
            <p className={classes.TextAfterImg}>{texts.addVideo}</p>
          </Grid>
          <Grid item xs={1} className={uiClasses.TextCenter}>
            <img src={arrowTopImg} alt="->" />
          </Grid>
          <Grid item xs={3} className={uiClasses.TextCenter}>
            <img className={classes.Img} src={moneyImg} alt="Money" />
            <p className={classes.TextAfterImg}>{texts.suggestMoney}</p>
          </Grid>
          <Grid item xs={1} className={uiClasses.TextCenter}>
            <img className={classes.ArrowBottom} src={arrowBottomImg} alt="->" />
          </Grid>
          <Grid item xs={3} className={uiClasses.TextCenter}>
            <img className={classes.Img} src={timeImg} alt="Time" />
            <p className={classes.TextAfterImg}>{texts.waitForCoachOffer}</p>
          </Grid>
        </Grid>
        <br /><br />
        <ButtonPrimary outline="true">{texts.startNow}</ButtonPrimary>
      </div>
    </React.Fragment>
  );
}

export default injectIntl(FindCoach);