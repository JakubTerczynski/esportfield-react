import React from 'react';
import { injectIntl } from 'react-intl';
import computerImg from '../../../assets/images/computer.svg';

const Marketplace = ({intl, ...props}) => {
  const texts = {
    marketplace: intl.formatMessage({
      id: 'common.marketplace'
    }),
  }
  return (
    <div>
      <h3 
        className='header-text-bold font-24px'
        style={{marginTop: '70px', marginLeft: '45px'}}>{texts.marketplace}</h3>
      <br /><br />
      <p 
        className="header-text-regular font-17px" 
        style={{marginLeft: '45px', lineHeight: '26px'}}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod.
      </p>
      <br /><br /><br />
      <img src={computerImg} alt="Computer"/>
  </div>
  );
}
 
export default injectIntl(Marketplace);