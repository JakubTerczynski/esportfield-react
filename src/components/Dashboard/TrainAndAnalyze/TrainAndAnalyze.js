import React from 'react';
import { injectIntl } from 'react-intl';
import ButtonPrimary from '../../UI/Buttons/ButtonPrimary/ButtonPrimary';

const TrainAndAnalyze = ({intl, ...props}) => {
  const texts = {
    trainAndAnalyze: intl.formatMessage({
      id: 'common.trainAndAnalyze'
    }),
    selectCoach: intl.formatMessage({
      id: 'coach.select'
    }),
    becomeCoach: intl.formatMessage({
      id: 'coach.become'
    }),
    chooseTheBestCoach: intl.formatMessage({
      id: 'coach.chooseTheBest'
    })
  };

  return (
    <div style={{marginTop: '30px'}}>
      <h1 className='header-text-bold font-55px'>{texts.trainAndAnalyze}</h1>
      <p className="header-text-regular font-17px"
        style={{marginRight: '30px', lineHeight: '153.9%'}}>
        {texts.chooseTheBestCoach}
      </p>
      <br /><br />
      <ButtonPrimary outline="true" style={{marginRight: '20px'}}>{texts.selectCoach}</ButtonPrimary>
      <ButtonPrimary outline="true">{texts.becomeCoach}</ButtonPrimary>
    </div>
  );
}

export default injectIntl(TrainAndAnalyze);