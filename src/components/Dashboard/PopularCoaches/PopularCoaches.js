import React from 'react';
import { Grid } from '@material-ui/core';
import CardWithAvatar from '../../UI/CardWithAvatar/CardWithAvatar';
import {StyledRating} from '../../UI/StyledRating/StyledRating';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import { injectIntl } from 'react-intl';
import ButtonSecondarySmall from '../../UI/Buttons/ButtonSecondary/ButtonSecondarySmall/ButtonSecondarySmall';
import ButtonSecondary from '../../UI/Buttons/ButtonSecondary/ButtonSecondary';
import earthImg from '../../../assets/images/earth.svg';

const PopularCoaches = ({intl, ...props}) => {
  const texts = {
    seeStats: intl.formatMessage({
      id: 'common.seeStats'
    }),
    seeAll: intl.formatMessage({
      id: 'common.seeAll'
    }),
    languages: intl.formatMessage({
      id: 'common.languages'
    }),
    coach: intl.formatMessage({
      id: 'coach'
    })
  };

  const coaches = popularCoaches.map(coach => (
    <Grid item xs={12} sm={6} key={Math.random()}>
      <CardWithAvatar
        labelText={texts.coach}
        labelColor="#A49620">
        <h3 className='MarginBottom5px'>{coach.nickname}</h3>
        <p className='NoMarginTop'>
          {coach.firstName}&nbsp;{coach.lastName}&nbsp;({coach.age})&nbsp;{coach.country}
        </p>
        <p className='NoMarginTop'>{coach.game}</p>
        <br />
        <StyledRating
          name="customized-empty"
          value={coach.rank}
          precision={0.5}
          emptyIcon={<StarBorderIcon style={{color: '#A49620'}} fontSize="inherit" />}
          readOnly />
        <p>
          <img src={earthImg} alt="Earth" />&nbsp;
          {texts.languages}: {coach.languages.join(', ')}
        </p>
        <br />
        <ButtonSecondarySmall>{texts.seeStats} >></ButtonSecondarySmall>
      </CardWithAvatar>
    </Grid>
  ));

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        {coaches}
      </Grid>
      <div style={{'padding': '30px', 'textAlign': 'center'}}>
        <ButtonSecondary outline="true">{texts.seeAll}</ButtonSecondary>
      </div>
    </React.Fragment>
  );
}

const defaultCoach = {
  nickname: 'HEAD HUNTER',
  firstName: 'Adam',
  lastName: 'Kowalski',
  age: 25,
  country: 'PL',
  game: 'Counter-Strike',
  rank: 4,
  languages: ['PL', 'EN']
}
const popularCoaches = [defaultCoach, defaultCoach];
 
export default injectIntl(PopularCoaches);