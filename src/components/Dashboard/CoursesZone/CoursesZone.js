import React from 'react';
import CoursesTabs from './CoursesTabs/CoursesTabs';
import { injectIntl } from 'react-intl';
import { Grid } from '@material-ui/core';

const CoursesZone = ({intl, ...props}) => {
  const texts = {
    coursesZone: intl.formatMessage({
      id: 'courses.zone'
    })
  };

  return (
    <React.Fragment>
      <div style={{ 'textAlign': 'center' }}>
        <h1 className='header-text-bold font-55px'
          style={{marginBottom: '20px'}}>{texts.coursesZone}</h1>
        <Grid container spacing={3} justify="center">
          <Grid item xs={6}>
            <p className="header-text-regular font-17px"
              style={{lineHeight: '25px'}}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod.
            </p>
          </Grid>
        </Grid>
      </div>
      <br /><br />
      <CoursesTabs/>
    </React.Fragment>
  );
}

export default injectIntl(CoursesZone);