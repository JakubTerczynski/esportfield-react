import React from 'react';
import { Card, CardMedia, CardContent, Grid } from '@material-ui/core';
import { StyledRating } from '../../../../UI/StyledRating/StyledRating';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import ButtonSecondary from '../../../../UI/Buttons/ButtonSecondary/ButtonSecondary';
import courseImg from '../../../../../assets/images/course.svg';

const CoursesTab = ({ intl, ...props }) => {
  const texts = {
    seeAll: intl.formatMessage({
      id: 'common.seeAll'
    })
  };

  const courses = props.courses.map(course => (
    <Grid item xs={12} sm={3} key={Math.random()}>
      <Card style={{ borderRadius: '10px' }}>
        <CardMedia
          style={{ width: '270px', height: '130px', borderRadius: '10px' }}
          image={courseImg} />
        <CardContent>
          <h3 className="header-text-bold font-17px">{course.name}</h3>
          <p className="header-text-regular font-14px NoMargin">{course.author}</p><br />
          <Grid container>
            <Grid item xs={7}>
              <StyledRating
                name="customized-empty"
                value={course.rate}
                precision={0.5}
                emptyIcon={<StarBorderIcon style={{ color: '#A49620' }} fontSize="inherit" />}
                readOnly />
            </Grid>
            <Grid item xs={4}>
              <p className="header-text-regular font-14px"
                style={{ margin: '5px' }}>
                <b>({course.rate})</b>&nbsp; {course.rateQuantity}
              </p>
            </Grid>
          </Grid>
          <br /><br />
          <p className="header-text-regular font-17px TextAlignRight NoMargin">
            <strike>59.99 zł</strike>&nbsp;&nbsp;
              <b>{course.price} zł</b>
          </p>
        </CardContent>
      </Card>
    </Grid>
  ));

  return (
    <React.Fragment>
      <Grid container spacing={3}>
        {courses}
      </Grid>
      <div style={{ 'padding': '10px', 'textAlign': 'center' }}>
        <ButtonSecondary outline="true">{texts.seeAll}</ButtonSecondary>
      </div>
    </React.Fragment>
  );
}

CoursesTab.propTypes = {
  courses: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    rate: PropTypes.number.isRequired,
    rateQuantity: PropTypes.number.isRequired,
    price: PropTypes.number.isRequired
  }))
}

export default injectIntl(CoursesTab);