import React, {useState} from 'react';
import { Tabs, Tab } from '@material-ui/core';
import TabPanel from './TabPanel/TabPanel';
import CoursesTab from './CoursesTab/CoursesTab';
import { makeStyles } from '@material-ui/styles';
import './CoursesTabs.scss';

const useStyles = makeStyles(theme => ({
  TabText: {
    fontFamily: 'Poppins-Regular',
    fontStyle: 'normal',
    fontWeight: 'bold',
    fontSize: '17px',
    lineHeight: '25px'
  }
}));

const CoursesTabs = (props) => {
  const [value, setValue] = useState(0);
  const uiClasses = useStyles();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  }

  return (
    <React.Fragment>
      <div style={{
          borderBottom: '1px solid #1479BD',
          marginBottom: '20px'
        }}>
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          centered
          className={uiClasses.TabText}>
          <Tab label="wideo" />
          <Tab label="tekstowe" />
        </Tabs>
      </div>
      <TabPanel value={value} index={0}>
        <CoursesTab courses={popularVideoCourses}/>
      </TabPanel>
      <TabPanel value={value} index={1}>
        <CoursesTab courses={popularTextCourses}/>
      </TabPanel>
    </React.Fragment>
  );
}

const defaultVideoCourse = {
  'name': 'Tytuł kursu wideo',
  'author': 'Adam Kowalski',
  'rate': 4,
  'rateQuantity': 21,
  'price': 29.99
}
const popularVideoCourses = [defaultVideoCourse, defaultVideoCourse, defaultVideoCourse, defaultVideoCourse];
const defaultTextCourse = {
  'name': 'Tytuł kursu tekstowego',
  'author': 'Jan Kowalski',
  'rate': 2,
  'rateQuantity': 14,
  'price': 29.99
}
const popularTextCourses = [defaultTextCourse, defaultTextCourse, defaultTextCourse, defaultTextCourse];
 
export default CoursesTabs;