import React from 'react';
import ButtonPrimary from '../../UI/Buttons/ButtonPrimary/ButtonPrimary';
import { injectIntl } from 'react-intl';

const JoinOrganization = ({intl, ...props}) => {
  const texts = {
    createAndJoinOrganization: intl.formatMessage({
      id: 'organization.createAndJoin'
    }),
    startNow: intl.formatMessage({
      id: 'common.startNow'
    })
  };

  return (
    <div style={{marginRight: '50px'}}>
      <br /><br />
      <h1 className='header-text-bold font-55px'>{texts.createAndJoinOrganization}</h1>
      <br />
      <p className="header-text-regular font-17px"
        style={{lineHeight: '25px'}}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod.
      </p>
      <br /><br />
      <ButtonPrimary outline="true">{texts.startNow}</ButtonPrimary>
    </div>
  );
}
 
export default injectIntl(JoinOrganization);