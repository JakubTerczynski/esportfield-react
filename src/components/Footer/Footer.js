import React from 'react';
import { Grid, Container, TextField } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import classes from './Footer.module.scss';
import fbImg from '../../assets/images/fb.svg';
import linkedinImg from '../../assets/images/linkedin.svg';
import instaImg from '../../assets/images/insta.svg';
import twitterImg from '../../assets/images/twitter.svg';
import logoImg from '../../assets/images/logo-footer.svg';
import ButtonSecondary from '../UI/Buttons/ButtonSecondary/ButtonSecondary';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  Input: {
    borderColor: '#0C7AAF',
    borderRadius: '10px',
    height: '49px',

    '&:hover': {
      borderColor: '#0C7AAF'
    }
  }
}));

const Footer = ({ intl, ...props }) => {
  const uiClasses = useStyles();

  const texts = {
    players: intl.formatMessage({
      id: 'players'
    }),
    training: intl.formatMessage({
      id: 'common.training'
    }),
    analysis: intl.formatMessage({
      id: 'common.analysis'
    }),
    organizations: intl.formatMessage({
      id: 'organizations'
    }),
    marketplace: intl.formatMessage({
      id: 'common.marketplace'
    }),
    stayWithUs: intl.formatMessage({
      id: 'common.stayWithUs'
    }),
    contactUs: intl.formatMessage({
      id: 'common.contactUs'
    }),
    subscribeNewsletter: intl.formatMessage({
      id: 'newsletter.subscribe'
    }),
    email: intl.formatMessage({
      id: 'common.email'
    }),
    subscribe: intl.formatMessage({
      id: 'common.subscribe'
    }),
    privacyPolicy: intl.formatMessage({
      id: 'common.privacyPolicy'
    }),
    support: intl.formatMessage({
      id: 'common.support'
    }),
    blog: intl.formatMessage({
      id: 'common.BLOG'
    })
  };

  return (
    <React.Fragment>
      <Container style={{ padding: '120px 10px' }}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={3}>
            <NavLink
              to="/"
              className={classes.FooterText}>
              {texts.players}
            </NavLink><br /><br />
            <NavLink
              to="/"
              className={classes.FooterText}>
              <b>{texts.training}/{texts.analysis}</b>
            </NavLink><br /><br />
            <NavLink
              to="/"
              className={classes.FooterText}>
              {texts.organizations}
            </NavLink><br /><br />
            <NavLink
              to="/"
              className={classes.FooterText}>
              {texts.marketplace}
            </NavLink><br /><br />
          </Grid>
          <Grid item xs={12} sm={4}>
            <p className={clsx(classes.FooterText, "NoMarginTop")}>{texts.stayWithUs}</p>
            <img className={classes.SocialMediaImg} src={fbImg} alt="FB" />
            <img className={classes.SocialMediaImg} src={linkedinImg} alt="LINKEDIN" />
            <img className={classes.SocialMediaImg} src={instaImg} alt="INSTAGRAM" />
            <img className={classes.SocialMediaImg} src={twitterImg} alt="TWITTER" />
            <br /><br />
            <ButtonSecondary outline="true">{texts.contactUs}</ButtonSecondary>
          </Grid>
          <Grid item xs={12} sm={5}>
            <p className={clsx(classes.FooterText, "NoMarginTop")}>{texts.subscribeNewsletter}</p>
            <br />
            <TextField
              InputProps={{
                classes: {
                  notchedOutline: uiClasses.Input
                }
              }}
              variant="outlined"
              value={''}
              onChange={() => { }}
              placeholder={texts.email} />
            <ButtonSecondary style={{ marginLeft: '-15px' }}>{texts.subscribe}</ButtonSecondary>
            <br /><br />
            <p style={{ opacity: 0.5 }}>
              <NavLink
                to="/"
                className={classes.FooterText}>
                {texts.privacyPolicy}
              </NavLink>&nbsp;&nbsp;|&nbsp;&nbsp;
              <NavLink
                to="/"
                className={classes.FooterText}>
                {texts.support} & FAQ
              </NavLink>&nbsp;&nbsp;|&nbsp;&nbsp;
              <NavLink
                to="/"
                className={classes.FooterText}>
                {texts.blog}
              </NavLink></p>
          </Grid>
        </Grid>
      </Container>
      <hr style={{ border: '1px solid #5F5F5F' }} />
      <Container style={{ padding: '80px 10px' }}>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <img src={logoImg} alt="ESPORTFIELD" />
          </Grid>
          <Grid item xs={6}>
            <p
              className="header-text-regular font-16px TextAlignRight NoMarginTop"
              style={{ opacity: 0.5 }}>
              2019 Esportfield. All rights reserved
            </p>
          </Grid>
        </Grid>
      </Container>
    </React.Fragment>
  );
}

export default injectIntl(Footer);