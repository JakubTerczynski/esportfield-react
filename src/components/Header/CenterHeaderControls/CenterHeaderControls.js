import React from 'react';
import { NavLink } from 'react-router-dom';
import { injectIntl } from 'react-intl';
import classes from './CenterHeaderControls.module.scss';

const CenterHeaderControls = ({ intl, ...props }) => {
  const texts = {
    players: intl.formatMessage({
      id: 'players'
    }),
    training: intl.formatMessage({
      id: 'common.training'
    }),
    analysis: intl.formatMessage({
      id: 'common.analysis'
    }),
    organizations: intl.formatMessage({
      id: 'organizations'
    }),
    marketplace: intl.formatMessage({
      id: 'common.marketplace'
    }),
  }
  return (
    <React.Fragment>
      <NavLink to="/" className={classes.NavLink}>{texts.players}</NavLink>
      <NavLink to="/" className={classes.NavLink}><b>{texts.training}/{texts.analysis}</b></NavLink>
      <NavLink to="/" className={classes.NavLink}>{texts.organizations}</NavLink>
      <NavLink to="/" className={classes.NavLink}>{texts.marketplace}</NavLink>
    </React.Fragment>
  );
}

export default injectIntl(CenterHeaderControls);