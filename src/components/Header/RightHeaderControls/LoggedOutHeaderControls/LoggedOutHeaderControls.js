import React from 'react';
import ButtonSecondary from '../../../UI/Buttons/ButtonSecondary/ButtonSecondary';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import * as actions from '../../../../store/actions/index';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import classes from './LoggedOutHeaderControls.module.scss';

const LoggedOutHeaderControls = ({ intl, ...props }) => {
  const texts = {
    login: intl.formatMessage({
      id: 'header.login'
    }),
    register: intl.formatMessage({
      id: 'header.register'
    })
  };

  return (
    <React.Fragment>
      <NavLink to="/login" 
        className={classes.NavLink}>{texts.login}</NavLink>
      <ButtonSecondary
        outline="true"
        onClick={props.onOpenRegisterModal}
        style={{ fontWeight: 'normal' }}>{texts.register}</ButtonSecondary>
    </React.Fragment>
  );
}

LoggedOutHeaderControls.propTypes = {
  onOpenRegisterModal: PropTypes.func
}

const mapDispatchToProps = dispatch => {
  return {
    onOpenRegisterModal: () => dispatch(actions.openRegisterModal()),
  }
}

export default connect(null, mapDispatchToProps)(injectIntl(LoggedOutHeaderControls));