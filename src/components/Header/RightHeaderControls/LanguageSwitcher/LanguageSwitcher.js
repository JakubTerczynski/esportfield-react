import React from 'react';
import { FormControl, Select, MenuItem } from '@material-ui/core';
import { connect } from 'react-redux';
import * as actions from '../../../../store/actions/index';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles(theme => ({
  root: {
    color: '#FFFFFF',
    fontFamily: 'Poppins-Regular',
    fontSize: '14px',
    lineHeight: '21px',
    marginLeft: '15px',
    marginTop: '5px',

    '&::before': {
      borderBottom: 'none'
    },
    '&::after': {
      borderBottom: 'none'
    },
    '&::hover': {
      borderBottom: 'none'
    }
  }
}));

const LanguageSwitcher = (props) => {
  const uiClasses = useStyles();

  const changeLanguage = (event) => {
    props.onChangeLanguage(event.target.value);
  }

  return (
    <FormControl>
      <Select
        value={props.language}
        onChange={changeLanguage}
        name="language"
        className={uiClasses.root}
        IconComponent={() => (<span />)}>
        <MenuItem value={'en'}>EN</MenuItem>
        <MenuItem value={'pl'}>PL</MenuItem>
      </Select>
    </FormControl>
  );
}

LanguageSwitcher.propTypes = {
  language: PropTypes.string,
  onChangeLanguage: PropTypes.func
}

const mapStateToProps = state => {
  return {
    language: state.lang.language
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onChangeLanguage: (language) => dispatch(actions.changeLanguage(language)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSwitcher);