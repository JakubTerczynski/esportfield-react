import React from 'react';
import LanguageSwitcher from './LanguageSwitcher/LanguageSwitcher';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import LoggedOutHeaderControls from './LoggedOutHeaderControls/LoggedOutHeaderControls';
import LoggedInHeaderControls from './LoggedInHeaderControls/LoggedInHeaderControls';

const RightHeaderControls = (props) => {
  return (
    <React.Fragment>
      {props.isAuth ? <LoggedInHeaderControls /> : <LoggedOutHeaderControls />}
      <LanguageSwitcher />
    </React.Fragment>
  );
}

RightHeaderControls.propTypes = {
  isAuth: PropTypes.bool.isRequired
}

const mapStateToProps = state => {
  return {
    isAuth: state.auth.token !== null
  }
}

export default connect(mapStateToProps)(RightHeaderControls);