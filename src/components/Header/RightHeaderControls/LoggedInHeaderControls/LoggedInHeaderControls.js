import React, { useState, useEffect } from 'react';
import { IconButton, Menu, MenuItem } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import ringImg from '../../../../assets/images/ring.svg';
import arrowTopImg from '../../../../assets/images/avatar-arrow-top.svg';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { injectIntl } from 'react-intl';
import {connect} from 'react-redux';
import * as actions from '../../../../store/actions/index';
import PropTypes from 'prop-types';
import headerClasses from '../../Header.module.scss';

const LoggedInHeaderControls = ({ intl, ...props }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [videoAnalysisId, setVideoAnalysisId] = useState(null);
  const onGetVideoAnalysisByCoachId = props.onGetVideoAnalysisByCoachId;
  const onCreateVideoAnalysis = props.onCreateVideoAnalysis;

  useEffect(() => {
    onGetVideoAnalysisByCoachId(props.userId);
  }, [onGetVideoAnalysisByCoachId, props.userId]);

  useEffect(() => {
    if (props.videoAnalysisByCoachId) {
      if (props.videoAnalysisByCoachId.length > 0) {
        setVideoAnalysisId(props.videoAnalysisByCoachId[0].id);
      } else {
        onCreateVideoAnalysis('2', '', '', props.userId);
      }
    }
  }, [props.videoAnalysisByCoachId, onCreateVideoAnalysis, props.userId]);

  useEffect(() => {
    if (props.videoAnalysis) {
      setVideoAnalysisId(props.videoAnalysis.id);
    }
  }, [props.videoAnalysis]);

  const texts = {
    logout: intl.formatMessage({
      id: 'header.logout'
    }),
    videoAnalysis: intl.formatMessage({
      id: 'video.analysis'
    }),
    videoAnalysisView: intl.formatMessage({
      id: 'video.analysisView'
    })
  };


  return (
    <React.Fragment>
      <img src={ringImg} alt="ring" style={{ marginRight: '15px' }} />
      <IconButton
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={(event) => setAnchorEl(event.currentTarget)}
        color="inherit"
        style={{ padding: '0px' }}>
        <AccountCircle style={{ width: '37px', height: '37px' }} />
        <img src={arrowTopImg} alt="arrow" style={{ marginLeft: '5px' }} />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        keepMounted
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}>
        <MenuItem>
          <NavLink 
            className={headerClasses.MenuItem}
            to={`/video-analysis/${videoAnalysisId}`} 
            onClick={() => setAnchorEl(null)}>{texts.videoAnalysis}</NavLink>
        </MenuItem>
        <MenuItem>
          <NavLink 
            className={headerClasses.MenuItem}
            to={`/video-analysis-view/${videoAnalysisId}`} 
            onClick={() => setAnchorEl(null)}>{texts.videoAnalysisView}</NavLink>
        </MenuItem>
        <MenuItem>
          <NavLink 
            className={headerClasses.MenuItem}
            to="/logout" 
            onClick={() => setAnchorEl(null)}>{texts.logout}</NavLink>
        </MenuItem>
      </Menu>
    </React.Fragment>
  );
}

LoggedInHeaderControls.propTypes = {
  userId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  videoAnalysis: PropTypes.object,
  videoAnalysisByCoachId: PropTypes.array,
  onGetVideoAnalysisByCoachId: PropTypes.func,
  onCreateVideoAnalysis: PropTypes.func
}

const mapStateToProps = state => {
  return {
    userId: state.auth.userId,
    videoAnalysis: state.videoAnalysis.videoAnalysis,
    videoAnalysisByCoachId: state.videoAnalysis.videoAnalysisByCoachId
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onGetVideoAnalysisByCoachId: (coachId) => dispatch(actions.getVideoAnalysisByCoachId(coachId)),
    onCreateVideoAnalysis: (videoId, summary, recommendation, coachId) => dispatch(actions.createVideoAnalysis(videoId, summary, recommendation, coachId)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(LoggedInHeaderControls));