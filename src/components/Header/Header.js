import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import LeftHeaderControls from './LeftHeaderControls/LeftHeaderControls';
import CenterHeaderControls from './CenterHeaderControls/CenterHeaderControls';
import RightHeaderControls from './RightHeaderControls/RightHeaderControls';
import { Container, Typography } from '@material-ui/core';

const Header = (props) => {
  return (
    <AppBar style={{boxShadow: 'none', backgroundColor: '#0A0A0A'}}>
      <Toolbar>
        <Container style={{display: 'flex'}} >
          <LeftHeaderControls />
          <Typography 
            style={{flexGrow: '1', textAlign: 'center', marginTop: '10px'}}>
            <CenterHeaderControls />
            </Typography>
          <RightHeaderControls />
        </Container>
      </Toolbar>
    </AppBar>
  );
}

export default Header;