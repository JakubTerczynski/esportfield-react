import React from 'react';
import logoImg from '../../../assets/images/logo-header.svg';
import {NavLink} from 'react-router-dom';
import GameSwitcher from './GameSwitcher/GameSwitcher';

const LeftHeaderControls = (props) => {
  return (
    <React.Fragment>
      <NavLink to="/">
        <img 
          src={logoImg}
          style={{marginTop: '10px'}} 
          alt="ESPORTFIELD" />
      </NavLink>
      <GameSwitcher />
    </React.Fragment>
  );
}

export default LeftHeaderControls;