import React from 'react';
import { injectIntl } from 'react-intl';
import { FormControl, Select, MenuItem, OutlinedInput } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import './GameSwitcher.scss';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import headerClasses from '../../Header.module.scss';

const useStyles = makeStyles(theme => ({
  root: {
    color: '#FFFFFF',
    fontFamily: 'Poppins-Regular',
    fontSize: '14px',
    lineHeight: '21px',
    marginLeft: '15px',
    marginTop: '5px',
    textTransform: 'lowercase'
  }
}));
const GameSwitcher = ({ intl, ...props }) => {
  const uiClasses = useStyles();

  const texts = {
    games: intl.formatMessage({
      id: 'games'
    })
  };

  const changeGame = (event) => {

  }

  let games = null;
  if (props.games) {
    games = props.games.map(game => <MenuItem
      key={game.id}
      value={game.id}
      className={headerClasses.MenuItem}>{game.name}</MenuItem>);
  }

  return (
    <FormControl variant="outlined" className="GameSwitcher">
      <Select
        value={0}
        onChange={changeGame}
        name="language"
        className={uiClasses.root}
        input={<OutlinedInput />}>
        <MenuItem value={0}>{texts.games}</MenuItem>
        {games}
      </Select>
    </FormControl>
  );
}

GameSwitcher.propTypes = {
  games: PropTypes.array
}

const mapStateToProps = state => {
  return {
    games: state.games.games,
  }
}

export default connect(mapStateToProps)(injectIntl(GameSwitcher));