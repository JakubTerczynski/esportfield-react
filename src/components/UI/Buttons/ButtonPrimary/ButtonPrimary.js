import React from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  Primary: {
    boxShadow: '0px 5px 15px rgba(14, 118, 188, 0.56)',
    borderRadius: '15px',
    fontFamily: 'Poppins-Regular',
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: '14px',
    lineHeight: '21px',
    color: '#FFFFFF',
    textTransform: 'inherit',
    padding: 0,
    minHeight: 60,
    minWidth: 200,
    border: '1px solid #0E76BC',

    '&:hover': {
      border: '1px solid #0E76BC',
      background: '#0E76BC'
    }
  },
  Contained: {
    border: '1px solid #0E76BC',
    background: '#0E76BC'
  }
}));

const ButtonPrimary = (props) => {
  const uiClasses = useStyles();

  return (
    <Button 
      {...props}
      variant={props.outline ? 'outlined' : 'contained'}  
      size="large" 
      color="primary"
      fullWidth={!!props.isfullwidth}
      className={clsx(uiClasses.Primary, {[uiClasses.Contained]: !props.outline})}>{props.children}</Button>
  );
}

ButtonPrimary.propTypes = {
  isfullwidth: PropTypes.string,
  outline: PropTypes.string,
  children: PropTypes.node
}

export default ButtonPrimary;