import React from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  Secondary: {
    borderRadius: '10px',
    fontFamily: 'Poppins-Regular',
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: '14px',
    lineHeight: '21px',
    color: '#FFFFFF',
    textTransform: 'inherit',
    padding: 0,
    minHeight: 45,
    minWidth: 150,
    border: '1px solid #0E76BC',

    '&:hover': {
      border: '1px solid #0E76BC',
      background: '#0E76BC'
    },
  }, 
  label: {
    padding: '10px'
  },
  Contained: {
    border: '1px solid #0E76BC',
    background: '#0E76BC'
  },
  Disabled: {
    border: '1px solid #0E76BC',
    background: '#0E76BC !important',
    color: '#FFFFFF !important',
    opacity: 0.5
  }
}));

const ButtonSecondary = (props) => {
  const uiClasses = useStyles();

  return (
    <Button 
      {...props}
      variant={props.outline ? 'outlined' : 'contained'} 
      size="medium" 
      color="primary"
      fullWidth={props.isfullwidth}
      className={clsx(
        uiClasses.Secondary, 
        uiClasses.label, 
        {[uiClasses.Contained]: !props.outline},
        {[uiClasses.Disabled]: props.disabled},
        props.className)}>
      {props.children}</Button>
  );
}

ButtonSecondary.propTypes = {
  isfullwidth: PropTypes.bool,
  outline: PropTypes.string,
  children: PropTypes.node
}

export default ButtonSecondary;