import React from 'react';
import { Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  SecondarySmall: {
    fontFamily: 'Poppins-Regular',
    fontStyle: 'normal',
    fontWeight: 600,
    fontSize: '14px',
    lineHeight: '21px',
    color: '#0E76BC',
    textTransform: 'inherit'
  }
}));

const ButtonSecondarySmall = (props) => {
  const uiClasses = useStyles();

  return (
    <Button 
      {...props}
      color="primary"
      className={uiClasses.SecondarySmall}>{props.children}</Button>
  );
}

export default ButtonSecondarySmall;