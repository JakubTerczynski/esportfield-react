import React from 'react';
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  TextField: {
    fontFamily: 'Poppins-Regular',
    fontStyle: 'normal',
    fontWeight: 'normal',
    fontSize: '14px',

    '& fieldset': {
      borderRadius: '10px'
    }
  }
}));

const StyledInput = (props) => {
  const uiClasses = useStyles();

  return (
    <TextField
      className={uiClasses.TextField}
      label={props.label}
      type={props.type}
      name={props.name}
      margin="normal"
      variant="outlined"
      value={props.value}
      onChange={props.onChange}
      InputProps={{ classes: { input: uiClasses.TextField } }}
      InputLabelProps={{ classes: { root: uiClasses.TextField } }}
      fullWidth
      {...props} />
  );
}

StyledInput.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired
}

export default StyledInput;