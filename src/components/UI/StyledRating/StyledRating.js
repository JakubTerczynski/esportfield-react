import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';

export const StyledRating = withStyles({
  iconFilled: {
    color: '#A49620',
  },
  iconHover: {
    color: '#A49620',
  },
})(Rating);

// const ColoredRating = (props) => {
//   return (  );
// }
 
// export default ColoredRating;