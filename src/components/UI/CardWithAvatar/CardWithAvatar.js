import React from 'react';
import { Card, CardContent, Avatar } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import classes from './CardWithAvatar.module.scss';
import clsx from 'clsx';
import PropTypes from 'prop-types';

const useStyles = makeStyles({
  BigAvatar: {
    width: 120,
    height: 120
  },
  AccountIcon: {
    fontSize: 60
  }
});

const CardWithAvatar = (props) => {
  const materialUIClasses = useStyles();

  return (
    <React.Fragment>
      <Avatar className={clsx(materialUIClasses.BigAvatar, classes.Avatar)}
        src="https://s3-alpha-sig.figma.com/img/4299/f0b0/da9e4e33f5b65adec3dbfa873b53874f?Expires=1567382400&Signature=VLzR75VnBENiz8v7Hcjm0DZzBeqmDJfvJ~UjdfHaqwmXYL2f~bIk9H7L3-uqt7g427eyXpeElc456sSBJnDQ5qvIz3xb0J-HshevYlo~DCUXUxwXyvoETPulHYfyzkss383KZJCfVamSsomD1j-kXMXavmEAlDo0jVs~t2Q3yW6gKFpDzMd0et1PX1I25PTJ~LPZRf9CcppJEiuqUOP9c0S85~GTc5~F4Y6-9~pLO~2gxzt1x43WuuQCHblO3wELWHdLhgKV6WfoIbKh86gHquSc6jE7tyT30n5DgFmGMyoDdunMs-aI31CEGicZeYdJWiGG27CE~~RH3Z1pHXGL8w__&Key-Pair-Id=APKAINTVSUGEWH5XD5UA" />
      <Card style={{borderRadius: '10px'}}>
        <div className={classes.Label} style={{backgroundColor: props.labelColor}}>
          {props.labelText}
        </div>
        <CardContent className={classes.CardContent} style={{padding: '20px'}}>
          {props.children}
        </CardContent>
      </Card>
    </React.Fragment>
  );
}

CardWithAvatar.propTypes = {
  children: PropTypes.node.isRequired,
  labelText: PropTypes.string.isRequired,
  labelColor: PropTypes.string.isRequired,
}

export default CardWithAvatar;