import axios from 'axios';

console.log(process.env.NODE_ENV);

const instance = axios.create({
  baseURL: 'https://esportfield.herokuapp.com/api/v1/',
  headers: { 'Access-Control-Allow-Origin': '*' }
});

export default instance;